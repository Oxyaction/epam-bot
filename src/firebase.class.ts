import * as config from 'config';
import firebase from 'firebase';

export class Firebase {

  private db: firebase.firestore.Firestore;

  public init(): void {
    firebase.initializeApp(config.get('firebase'));
    this.db = firebase.firestore();
  }

  public close(): void {
    firebase.firestore().disableNetwork();
  }


  public write(collection: string, key: string, data: any): Promise<void> {
    const doc = this.db.collection(collection).doc(key);
    return doc.set(data);
  }

  public async remove(collection: string, key: string): Promise<void> {
    await this.db.collection(collection).doc(key).delete();
  }

  public read(collection: string) {
    return this.db.collection(collection);
  }
}
