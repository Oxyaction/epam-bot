import { Firebase } from '../firebase.class';
import * as argv from 'minimist';

const options = argv(process.argv.slice(2), {
  default: {
    limit: 10,
    collection: 'logs',
    dead: false, // get only dead logs
  },
});

async function getLogs(fb: Firebase) {
  const result = {};

  const snapshot = await fb.read(options.collection).orderBy('date', 'desc').limit(options.limit).get();

  snapshot.forEach((doc) => {
    result[doc.id] = doc.data();
  });

  await Promise.all(Object.keys(result).map((docId) => {
    return fb.remove(options.collection, docId);
  }));

  return result;
}

function formatLogs(logs) {
  return {
    sessions: Object.keys(logs).reduce((prev, text) => {
      prev[text] = JSON.parse(logs[text].logs);
      return prev;
    }, {}),
  }
}

(async function () {
  const fb = new Firebase();
  fb.init();

  let logs = await getLogs(fb);
  logs = formatLogs(logs);
  console.log(JSON.stringify(logs));
  fb.close()
})();
