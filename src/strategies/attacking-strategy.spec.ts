import {
  AttackingStrategy
} from './attacking-strategy.class'
import { SnakeState } from '../snake.state.class';
import { Board, Coordinates } from '../board.class';
import { Snake } from '../snake.class';
import { Pathfinder } from '../pathfinder.class';

describe('AttackingStrategy', () => {
  const getStrategyResolution = (boardAsString: string, state?: SnakeState): Coordinates[] => {
    const board = new Board(boardAsString);
    const player: Snake = board.getPlayer();
    if (state) {
      player.setState(state);
    } else {
      player.setState(new SnakeState());
    }
    const strategy = new AttackingStrategy(board, new Pathfinder());
    return strategy.evaluateBoard(player);
  }

  describe('should attack closest nearby enemy', () => {
    it('in neck', () => {
      const board =
        '☼☼☼☼☼☼☼☼' +
        '☼     ○☼' +
        '☼  ╘═♥˄☼' +
        '☼     │☼' +
        '☼     │☼' +
        '☼     │☼' +
        '☼×────┘☼' +
        '☼☼☼☼☼☼☼☼';
      const state: SnakeState = new SnakeState();
      state.furyDuration = 1;
      const path: Coordinates[] = getStrategyResolution(board, state);
      expect(path).toEqual([{ x: 6, y: 2 }]);
    });

    it('in tail', () => {
      const board =
        '☼☼☼☼☼☼☼☼' +
        '☼     ○☼' +
        '☼     ˄☼' +
        '☼     │☼' +
        '☼  ╘═♥│☼' +
        '☼     │☼' +
        '☼×────┘☼' +
        '☼☼☼☼☼☼☼☼';
      const state: SnakeState = new SnakeState();
      state.furyDuration = 1;
      const path: Coordinates[] = getStrategyResolution(board, state);
      expect(path).toEqual([{ x: 6, y: 4 }]);
    });

    it('should prioritize neck attack over tail attack', () => {
      const board =
        '☼☼☼☼☼☼☼☼' +
        '☼      ☼' +
        '☼      ☼' +
        '☼    <┐☼' +
        '☼  ╘═♥│☼' +
        '☼     │☼' +
        '☼×────┘☼' +
        '☼☼☼☼☼☼☼☼';
      const state: SnakeState = new SnakeState();
      state.furyDuration = 1;
      const path: Coordinates[] = getStrategyResolution(board, state);
      expect(path).toEqual([{ x: 5, y: 3 }]);
    });

    it('if bigger than enemy', () => {
      const board =
        '☼☼☼☼☼☼☼☼' +
        '☼     ○☼' +
        '☼ ╔══►˄☼' +
        '☼ ║   ¤☼' +
        '☼ ║    ☼' +
        '☼ ╙    ☼' +
        '☼      ☼' +
        '☼☼☼☼☼☼☼☼';
      const state: SnakeState = new SnakeState();
      state.furyDuration = 1;
      const path: Coordinates[] = getStrategyResolution(board, state);
      expect(path).toEqual([{ x: 6, y: 2 }]);
    });
  });

  describe('should not attack', () => {
    it('if dies after attack', () => {
      const board =
        '☼☼☼☼☼☼☼☼' +
        '☼     ○☼' +
        '☼  ╘═►˄☼' +
        '☼     ¤☼' +
        '☼      ☼' +
        '☼      ☼' +
        '☼      ☼' +
        '☼☼☼☼☼☼☼☼';
      const path: Coordinates[] = getStrategyResolution(board);
      expect(path).toHaveLength(0);
    });

    it('if fury not enough', () => {
      const board =
        '☼☼☼☼☼☼☼☼' +
        '☼     ○☼' +
        '☼  ╘═♥˄☼' +
        '☼     ¤☼' +
        '☼      ☼' +
        '☼      ☼' +
        '☼      ☼' +
        '☼☼☼☼☼☼☼☼';
      const state: SnakeState = new SnakeState();
      state.furyDuration = 0;
      const path: Coordinates[] = getStrategyResolution(board);
      expect(path).toHaveLength(0);
    });

    it('should not consider dead snake as target', () => {
      const board = "☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼" +
        "☼☼                           ☼" +
        "☼#                           ☼" +
        "☼☼       ●        ×──┐       ☼" +
        "☼☼                   │       ☼" +
        "☼☼           ●      <◄╗      ☼" +
        "☼☼     ☼☼☼☼☼    ╔═════╝      ☼" +
        "☼☼     ☼        ╚╗           ☼" +
        "☼#     ☼☼☼  ╔════╝☼☼☼☼#      ☼" +
        "☼☼     ☼    ╙     ☼  ○☼  ●   ☼" +
        "☼☼     ☼☼☼☼#      ☼☼☼☼#  ○   ☼" +
        "☼☼                ☼          ☼" +
        "☼☼                ☼          ☼" +
        "☼☼    ●                      ☼" +
        "☼#                    ○ ○    ☼" +
        "☼☼                           ☼" +
        "☼☼        ☼☼☼              ○ ☼" +
        "☼☼       ☼  ☼        ●     ○ ☼" +
        "☼☼      ☼☼☼☼#    $☼☼   ☼# ○® ☼" +
        "☼☼      ☼   ☼   ● ☼ ☼ ☼ ☼    ☼" +
        "☼#      ☼ © ☼     ☼  ☼  ☼    ☼" +
        "☼☼              ● ☼     ☼    ☼" +
        "☼☼     ●          ☼     ☼    ☼" +
        "☼☼                           ☼" +
        "☼☼         ○                 ☼" +
        "☼☼ ○    ○      ●  ●          ☼" +
        "☼#                           ☼" +
        "☼☼        ●                  ☼" +
        "☼☼                           ☼" +
        "☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼";

      const path: Coordinates[] = getStrategyResolution(board);
      expect(path).toHaveLength(0);
    });

    it('should not go to the wall', () => {
      const board = "☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼                           ☼☼#   ®                       ☼☼☼       ●                   ☼☼☼                           ☼☼☼           ●               ☼☼☼     ☼☼☼☼☼                 ☼☼☼     ☼        ┌┐           ☼☼#     ☼☼☼     ┌┘│☼☼☼☼#      ☼☼☼     ☼      ┌┘ ˅☼  ○☼  ●   ☼☼☼    æ☼☼☼☼#  ¤   ☼☼☼☼#      ☼☼☼    │           ☼          ☼☼☼    └┐          ☼          ☼☼☼   ●●│                     ☼☼#     │                  ○  ☼☼☼     │ ○                   ☼☼☼®    ˅  ☼☼☼                ☼☼☼       ☼  ☼                ☼☼☼      ☼☼☼☼#     ☼☼   ☼#    ☼☼☼$     ☼   ☼   ● ☼ ☼ ☼ ☼    ☼☼#    ○ ☼   ☼     ☼  ☼  ☼    ☼☼☼      $         ☼     ☼    ☼☼☼ ®   ●          ☼     ☼    ☼☼☼             ○             ☼☼☼☺                          ☼☼☼◄══╗         ●             ☼☼#│  ╚══╕                    ☼☼☼└ö                       ○ ☼☼☼                           ☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼";
      const path: Coordinates[] = getStrategyResolution(board);
      expect(path).toHaveLength(0);
    });
  });
});
