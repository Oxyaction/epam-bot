import { SneakingAroundStrategy } from './sneaking-aroundy-strategy.class';
import { Board, Coordinates, BoardObject } from '../board.class';
import { Snake } from '../snake.class';
import { Pathfinder } from '../pathfinder.class';
import { SnakeState } from '../snake.state.class';

describe.skip('SneakingAroundStrategy', () => {

  const getStrategyResolution = (boardAsString: string, state?: SnakeState): { board: Board, path: Coordinates[], player } => {
    const board = new Board(boardAsString);
    const player: Snake = board.getPlayer();
    if (state) {
      player.setState(state);
    } else {
      player.setState(new SnakeState());
    }
    const strategy = new SneakingAroundStrategy(board, new Pathfinder());
    const path = strategy.evaluateBoard(player);
    return {
      board,
      path,
      player,
    }
  }

  it('should not go in front of stronger enemy', () => {

  });
});
