import { Board, Coordinates } from "../board.class";
import { DecisionManager } from './decision-manager.class';
import { Pathfinder } from "../pathfinder.class";
import { SnakeState } from "../snake.state.class";
import { Snake } from "../snake.class";

describe('DicisionManager', () => {

  const decide = (boardAsString: string, state?: SnakeState): { path: Coordinates[], act: boolean } => {
    const board: Board = new Board(boardAsString);
    const player: Snake = board.getPlayer();
    if (state) {
      player.setState(state);
    } else {
      player.setState(new SnakeState());
    }
    const pf: Pathfinder = new Pathfinder();
    const dm = new DecisionManager(board, pf);
    return dm.decide(board.getPlayer());
  }

  it.only('why not attack', () => {
    const board =
      '☼☼☼☼☼☼☼☼' +
      '☼      ☼' +
      '☼  ╘═♥ ☼' +
      '☼      ☼' +
      '☼      ☼' +
      '☼      ☼' +
      '☼      ☼' +
      '☼☼☼☼☼☼☼☼';
    const result = decide(board, new SnakeState({ furyDuration: 5, stonesCount: 5 }));
    expect(result.act).toBeTruthy();
    expect(result.path).toEqual([{ "x": 5, "y": 1 }, { "x": 4, "y": 1 }, { "x": 3, "y": 1 }, { "x": 3, "y": 2 }])
  });
});
