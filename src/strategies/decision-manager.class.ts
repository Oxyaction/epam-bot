import { Board, Coordinates } from "../board.class";
import { Pathfinder } from "../pathfinder.class";
import { Logger } from "../logger/logger.class";
import { Snake } from "src/snake.class";
import { EatingStrategy } from "./eating-strategy.class";
import { InterceptionStrategy } from "./interception-strategy.class";
import { AbstractStrategy } from "./abstract-strategy.class";
import { AttackingStrategy } from "./attacking-strategy.class";
import { StoneEatingStrategy } from "./stone-eating-strategy.class";
import { SneakingAroundStrategy } from "./sneaking-aroundy-strategy.class";

export class DecisionManager {
  constructor(private board: Board, private pathfinder: Pathfinder, private debugMessages: string[] = [], private logger?: Logger) { }

  public decide(snake: Snake): { path: Coordinates[], act: boolean } {
    let { path, strategyName, act } = this.getPathByPathLength(snake);

    if (path.length) {
      this.debugMessages.push(`${strategyName} path was chosen`);
      this.logger && this.logger.debug('strategy', strategyName);
    } else {
      this.debugMessages.push(`no way found, moving in the same direction`);
      const coordinates = Board.commandToCoordinates(snake.head.coordinates, snake.getState().previousCommand);
      path = [coordinates];
    }

    return { path, act };
  }

  private getPathByStrategyPriority(snake: Snake): { path: Coordinates[], strategyName: string } {
    const attackingStrategy: AbstractStrategy = new AttackingStrategy(this.board, this.pathfinder);
    const eatingStrategy: AbstractStrategy = new EatingStrategy(this.board, this.pathfinder);
    const interceptionStrategy: AbstractStrategy = new InterceptionStrategy(this.board, this.pathfinder);

    const strategies: AbstractStrategy[] = [attackingStrategy, interceptionStrategy, eatingStrategy];

    let path: Coordinates[] = [];
    let strategyName: string;
    let i = 0;
    do {
      path = strategies[i].evaluateBoard(snake);
      strategyName = strategies[i].name;
      i++;
    } while (!path.length && i < strategies.length);
    return {
      path,
      strategyName,
    };
  }

  private getPathByPathLength(snake: Snake): { path: Coordinates[], strategyName: string, act: boolean } {
    const attackingStrategy: AbstractStrategy = new AttackingStrategy(this.board, this.pathfinder);
    const eatingStrategy: AbstractStrategy = new EatingStrategy(this.board, this.pathfinder);
    const interceptionStrategy: AbstractStrategy = new InterceptionStrategy(this.board, this.pathfinder);
    const stoneEatingStrategy: AbstractStrategy = new StoneEatingStrategy(this.board, this.pathfinder);
    const sneakingAroundStrategy: AbstractStrategy = new SneakingAroundStrategy(this.board, this.pathfinder);

    let path: Coordinates[] = attackingStrategy.evaluateBoard(snake);
    // TRYING TO ATTACK FIRST
    if (path.length) {
      return {
        path,
        strategyName: attackingStrategy.name,
        act: false,
      }
    }
    const interceptionPath = interceptionStrategy.evaluateBoard(snake);

    const eatingPath: Coordinates[] = eatingStrategy.evaluateBoard(snake);
    // TRYING TO INTERCEPT IF PATH IS BETTER THAN EATING
    if (interceptionPath.length && ((interceptionPath.length < 15 && eatingPath.length > 3) || eatingPath.length > 10)) {
      return {
        path: interceptionPath,
        strategyName: interceptionStrategy.name,
        act: false,
      }
    }

    const stoneEatingPath: Coordinates[] = stoneEatingStrategy.evaluateBoard(snake);

    // TRYING TO EAT OWN STONES
    if ((!eatingPath.length && stoneEatingPath.length) || (stoneEatingPath.length < eatingPath.length && stoneEatingPath.length)) {
      snake.getState().decrementStone();
      return {
        path: stoneEatingPath,
        strategyName: stoneEatingStrategy.name,
        act: true,
      };
    }
    // EATING PATH
    if (eatingPath.length) {
      return {
        path: eatingPath,
        strategyName: eatingStrategy.name,
        act: false,
      };
    }

    // FALLBACK STRATEGY
    return {
      path: sneakingAroundStrategy.evaluateBoard(snake),
      strategyName: sneakingAroundStrategy.name,
      act: false,
    };
  }
}
