import { AbstractStrategy } from "./abstract-strategy.class";
import { Coordinates } from "../board.class";
import { Snake } from "src/snake.class";

export class StoneEatingStrategy extends AbstractStrategy {
  public get name(): string {
    return 'stone-eating';
  }

  public evaluateBoard(snake: Snake): Coordinates[] {
    if (!snake.getState().stonesCount) {
      return [];
    }
    const pathToTail: Coordinates[] = this.pathfinder.findPathToCoords(this.board, snake.tail.coordinates, snake.head.coordinates, snake);
    const strongestEnemy = this.board.getStrongestEnemy();
    if (!snake.canEatStone(pathToTail.length) || (!snake.isFury && strongestEnemy && snake.length - 3 <= strongestEnemy.length)) {
      return [];
    }
    return pathToTail;
  }
}
