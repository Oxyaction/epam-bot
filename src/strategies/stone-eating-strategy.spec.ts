import {
  StoneEatingStrategy
} from './stone-eating-strategy.class'
import { SnakeState } from '../snake.state.class';
import { Board, Coordinates } from '../board.class';
import { Snake } from '../snake.class';
import { Pathfinder } from '../pathfinder.class';

describe('StoneEatingStrategy', () => {
  const getStrategyResolution = (boardAsString: string, state?: SnakeState): Coordinates[] => {
    const board = new Board(boardAsString);
    const player: Snake = board.getPlayer();
    if (state) {
      player.setState(state);
    } else {
      player.setState(new SnakeState());
    }
    const strategy = new StoneEatingStrategy(board, new Pathfinder());
    return strategy.evaluateBoard(player);
  }

  describe('should return path', () => {
    it('if in fury and stone exists', () => {
      const board =
        '☼☼☼☼☼☼☼☼' +
        '☼      ☼' +
        '☼  ╘═♥ ☼' +
        '☼      ☼' +
        '☼      ☼' +
        '☼      ☼' +
        '☼      ☼' +
        '☼☼☼☼☼☼☼☼';
      const state: SnakeState = new SnakeState({ furyDuration: 5, stonesCount: 5 });
      const path: Coordinates[] = getStrategyResolution(board, state);
      expect(path).toHaveLength(4);
      expect(path[path.length - 1]).toEqual({ x: 3, y: 2 });
    });

    it('if not die after eating and stone exists', () => {
      const board =
        '☼☼☼☼☼☼☼☼' +
        '☼      ☼' +
        '☼ ╘═══►☼' +
        '☼      ☼' +
        '☼      ☼' +
        '☼      ☼' +
        '☼      ☼' +
        '☼☼☼☼☼☼☼☼';
      const state: SnakeState = new SnakeState({ stonesCount: 5 });
      const path: Coordinates[] = getStrategyResolution(board, state);
      expect(path).toHaveLength(6);
      expect(path[path.length - 1]).toEqual({ x: 2, y: 2 });
    });

    it('if will remain the strongest snake after eating', () => {
      const board =
        '☼☼☼☼☼☼☼☼' +
        '☼      ☼' +
        '☼╘════►☼' +
        '☼      ☼' +
        '☼      ☼' +
        '☼      ☼' +
        '☼ <ö   ☼' +
        '☼☼☼☼☼☼☼☼';
      const state: SnakeState = new SnakeState({ stonesCount: 5 });
      const path: Coordinates[] = getStrategyResolution(board, state);
      expect(path).toHaveLength(7);
      expect(path[path.length - 1]).toEqual({ x: 1, y: 2 });
    });

    it('if in fury and fury is enough despite enemy is stronger', () => {
      const board =
        '☼☼☼☼☼☼☼☼' +
        '☼      ☼' +
        '☼    ╘♥☼' +
        '☼      ☼' +
        '☼      ☼' +
        '☼      ☼' +
        '☼ <ö   ☼' +
        '☼☼☼☼☼☼☼☼';
      const state: SnakeState = new SnakeState({ furyDuration: 5, stonesCount: 5 });
      const path: Coordinates[] = getStrategyResolution(board, state);
      expect(path).toHaveLength(3);
      expect(path).toEqual([{ "x": 6, "y": 1 }, { "x": 5, "y": 1 }, { "x": 5, "y": 2 }]);
    });
  });

  describe('should not return path if', () => {
    it('no stones', () => {
      const board =
        '☼☼☼☼☼☼☼☼' +
        '☼      ☼' +
        '☼  ╘═♥ ☼' +
        '☼      ☼' +
        '☼      ☼' +
        '☼      ☼' +
        '☼      ☼' +
        '☼☼☼☼☼☼☼☼';
      const state: SnakeState = new SnakeState({ furyDuration: 5 });
      const path: Coordinates[] = getStrategyResolution(board, state);
      expect(path).toHaveLength(0);
    });

    it('fury not enough to reach stone', () => {
      const board =
        '☼☼☼☼☼☼☼☼' +
        '☼      ☼' +
        '☼  ╘═♥ ☼' +
        '☼      ☼' +
        '☼      ☼' +
        '☼      ☼' +
        '☼      ☼' +
        '☼☼☼☼☼☼☼☼';
      const state: SnakeState = new SnakeState({ furyDuration: 3, stonesCount: 5 });
      const path: Coordinates[] = getStrategyResolution(board, state);
      expect(path).toHaveLength(0);
    });

    it('will become equal or less than strongest enemy', () => {
      const board =
        '☼☼☼☼☼☼☼☼' +
        '☼      ☼' +
        '☼╘═══► ☼' +
        '☼      ☼' +
        '☼      ☼' +
        '☼      ☼' +
        '☼ <ö   ☼' +
        '☼☼☼☼☼☼☼☼';
      const state: SnakeState = new SnakeState({ stonesCount: 5 });
      const path: Coordinates[] = getStrategyResolution(board, state);
      expect(path).toHaveLength(0);
    });
  });
});
