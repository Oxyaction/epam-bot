import { Board, BoardObject, Coordinates, MovableFilter } from '../board.class';
import { AbstractStrategy } from './abstract-strategy.class';
import { AttackingStrategy } from './attacking-strategy.class';
import { InterceptionStrategy } from './interception-strategy.class';
import { Element, Command } from '../constants';
import { Snake } from '../snake.class';

type OptimalPath = {
  path: Coordinates[],
  target: BoardObject
}
export class EatingStrategy extends AbstractStrategy {

  public get name(): string {
    return 'eating';
  }

  public evaluateBoard(snake: Snake, checkPathSafey: boolean = true): Coordinates[] {
    let boardObjects: BoardObject[] = this.getEeatableSurround(this.board, snake, 20);
    boardObjects.forEach(this.evaluateSurround);
    boardObjects = this.sortSurround(boardObjects, snake);
    return this.findOptimalPath(boardObjects, snake, 10, checkPathSafey);
  }

  evaluateSurround(boardObject: BoardObject) {
    switch (boardObject.element) {
      case Element.NONE:
        boardObject.value = 0;
        break;
      case Element.FLYING_PILL:
        boardObject.value = 1;
        break;
      case Element.APPLE:
        boardObject.value = 2;
        break;
      case Element.FURY_PILL:
      // boardObject.value = 3;
      // break;
      case Element.STONE:
      case Element.GOLD:
        boardObject.value = 4;
        break;
      default:
        boardObject.value = -1;
    }
  }

  sortSurround(surround: BoardObject[], snake: Snake) {
    const snakeCoordinates: Coordinates = snake.head.coordinates;
    return surround.sort((a, b) => {
      if (a.value > b.value) {
        return -1
      } else if (a.value < b.value) {
        return 1;
      } else {
        const distanceToA = Board.countDistance(snakeCoordinates, a.coordinates);
        const distanceToB = Board.countDistance(snakeCoordinates, b.coordinates);
        if (distanceToA < distanceToB) {
          return -1
        } else if (distanceToB < distanceToA) {
          return 1;
        } else {
          const aCommand: Command = Board.coordinatesToCommand(a.coordinates, snake.head.coordinates);
          if (aCommand && aCommand === snake.direction) {
            return -1
          }
          const bCommand: Command = Board.coordinatesToCommand(b.coordinates, snake.head.coordinates);
          if (bCommand && bCommand === snake.direction) {
            return 1;
          }
        }
        return 0;
      }

      // return 0;
    });
  }

  private choseOptimalPath(paths: OptimalPath[], snake: Snake): Coordinates[] {
    // sort by distance
    paths = paths
      .sort((path1, path2) => {
        if (path1.path.length < path2.path.length && path1.target.value > 1) {
          return -1;
        }
        if (path2.path.length < path1.path.length && path2.target.value > 1) {
          return 1;
        }
        return 0;
      });
    // prioritize rocks for fury
    if (snake.isFury) {
      paths = paths.sort((path1, path2) => {
        if (snake.isPlayer) {
          if (path1.target.element === Element.FURY_PILL && snake.getState().stonesCount && snake.length < 5) {
            return -1;
          }
          if (path2.target.element === Element.FURY_PILL && snake.getState().stonesCount && snake.length < 5) {
            return 1;
          }
        }
        if (path1.target.element === Element.STONE && path2.target.element === Element.STONE) {
          if (path1.path.length < path2.path.length) {
            return -1;
          }
          if (path2.path.length < path1.path.length) {
            return 1;
          }
          return 0;
        }
        if (path1.target.element === Element.STONE && path2.target.element !== Element.STONE) {
          return -1;
        }
        if (path2.target.element === Element.STONE && path1.target.element !== Element.STONE) {
          return 1;
        }
        return 0;
      });
    }
    if (paths.length) {
      return paths[0].path;
    }
    return [];
  }

  private findOptimalPath(surroundings: BoardObject[], snake: Snake, numberOfTargets: number, checkPathSafey: boolean): Coordinates[] {
    const safeAvailablePaths: OptimalPath[] = [];
    const enemyStrategies = [
      new AttackingStrategy(this.board, this.pathfinder),
      new InterceptionStrategy(this.board, this.pathfinder),
      this,
    ];
    surroundings.slice(0, numberOfTargets).forEach((surround) => {
      const path = this.pathfinder.findPathToCoords(this.board, surround.coordinates, snake.head.coordinates, snake);
      if (
        path.length > 0
        && (
          !checkPathSafey
          || snake.isEnemy
          || (
            !this.pathfinder.isEnemyGetsEndOfPathFaster(this.board, path, enemyStrategies)
            && this.pathfinder.isSafePath(this.board, path, snake, enemyStrategies)
          )
        )
      ) {
      safeAvailablePaths.push({
        path,
        target: surround
      });
    }
  });

    return this.choseOptimalPath(safeAvailablePaths, snake);
  }

  private getEeatableSurround(board: Board, snake: Snake, limit: number): BoardObject[] {
  const headCoordinates: Coordinates = snake.head.coordinates;
  const surround: BoardObject[] = [];
  for (let i = -limit; i < limit + 1; i++) {
    for (let j = -limit; j < limit + 1; j++) {
      const newCoordinates: Coordinates = { x: headCoordinates.x + i, y: headCoordinates.y + j };
      if (board.isOut(newCoordinates)) {
        continue;
      }
      let element: Element = board.getElementByCoordinates(newCoordinates);

      if (this.isMovableElement(board, element, snake)) {
        surround.push({
          coordinates: newCoordinates,
          element
        });
      }
    }
  }
  return surround;
}

  private isMovableElement(board: Board, element: Element, snake: Snake): boolean {
  const elements = [
    Element.APPLE,
    Element.FLYING_PILL,
    Element.FURY_PILL,
    Element.GOLD,
    // Element.NONE
  ];
  if (snake.isEnemy && snake.isFury) {
    elements.push(Element.STONE);
  }
  if (snake.isPlayer) {
    const strongestEnemy: Snake = board.getStrongestEnemy();
    if (
      snake.isFury
      || (snake.length - 3 > 1 && (!strongestEnemy || snake.length - 3 > strongestEnemy.length))
    ) {
      elements.push(Element.STONE);
    }
  }

  return elements.includes(element);
}

}
