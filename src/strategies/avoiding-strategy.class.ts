import { AbstractStrategy } from "./abstract-strategy.class";
import { Coordinates, Board, BoardObject } from "../board.class";
import { Command } from "../constants";
import { Snake } from "../snake.class";
import { EatingStrategy } from "./eating-strategy.class";

export class AvoidingStrategy extends AbstractStrategy {
  public get name(): string {
    return 'avoiding';
  }

  public evaluateBoard(snake: Snake): Coordinates[] {
    return [{ x: 0, y: 0 }];
  }
}
