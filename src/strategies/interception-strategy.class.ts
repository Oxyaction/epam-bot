import { AbstractStrategy } from "./abstract-strategy.class";
import { EatingStrategy } from './eating-strategy.class';
import { Snake } from "../snake.class";
import { Coordinates, Board } from "../board.class";
import { Element } from "../constants";

type SnakePath = {
  path: Coordinates[],
  snake: Snake,
}

export class InterceptionStrategy extends AbstractStrategy {

  public get name(): string {
    return 'interception';
  }

  public evaluateBoard(snake: Snake): Coordinates[] {
    const enemyStrategy: AbstractStrategy = new EatingStrategy(this.board, this.pathfinder);
    const enemiesPaths: SnakePath[] = this.getEnemyPaths(snake, enemyStrategy);
    const interceptionPaths: Coordinates[][] = this.getInterceptionPaths(snake, enemiesPaths, enemyStrategy);
    if (interceptionPaths.length) {
      return interceptionPaths[0];
    }
    return [];
  }

  private getInterceptionPaths(snake: Snake, enemiesPaths: SnakePath[], enemyStrategy: AbstractStrategy): Coordinates[][] {
    let interceptionPaths: Coordinates[][] = [];
    for (let i = 0; i < enemiesPaths.length; i++) {
      if (!enemiesPaths[i].path.length) {
        continue;
      }
      let interceptionPath: Coordinates[]
        = this.pathfinder.interceptPath(this.board, snake, snake.head.coordinates, enemiesPaths[i].path);
      // try to go to the enemy tail
      const pathToEnemySegment = this.getPathToEnemySegment(snake, enemiesPaths[i].snake);
      if (!interceptionPath.length || (pathToEnemySegment.length && pathToEnemySegment.length < interceptionPath.length)) {
        interceptionPath = pathToEnemySegment;
      }

      // if no interception path than go to other snake's head (following path)
      if (!interceptionPath.length) {
        interceptionPath = this.pathfinder.findPathToCoords(this.board, enemiesPaths[i].snake.head.coordinates, snake.head.coordinates, snake);
      }
      if (
        interceptionPath.length
        && snake.isStrongerThen(enemiesPaths[i].snake, interceptionPath.length)
      ) {

        if (snake.isEnemy) {
          interceptionPaths.push(interceptionPath);
        } else {
          // Remove enemies tail to trace safe path back
          const newBoard: Board = this.board.copy();
          const targetCoordinateIdx = enemiesPaths[i].snake.coordinates
            .findIndex((coordinates) => Board.coordinatesEqual(interceptionPath[interceptionPath.length - 1], coordinates));
          enemiesPaths[i].snake.coordinates.forEach((coordinates, idx) => {
            if (idx >= targetCoordinateIdx) {
              newBoard.setElement(coordinates, Element.NONE);
            }
          });
          if (this.pathfinder.isSafePath(newBoard, interceptionPath, snake, [enemyStrategy])) {
            interceptionPaths.push(interceptionPath);
          }
        }
      }

    }

    interceptionPaths = interceptionPaths.sort((path1, path2) => {
      if (path1.length < path2.length) {
        return -1
      }
      if (path2.length < path1.length) {
        return 1;
      }
      return 0;
    });
    return interceptionPaths;
  }

  private getPathToEnemySegment(snake: Snake, enemySnake: Snake): Coordinates[] {
    let bestPathToEnemySegment: Coordinates[] = [];
    // let currentBiggestDamage = 0;
    if (snake.isPlayer && snake.isFury && snake.getState().furyDuration > 1 && !enemySnake.isFury) {
      enemySnake.coordinates.forEach((coordinates, segmentIdx) => {
        const pathToEnemySegment: Coordinates[] = this.pathfinder.findPathToCoords(this.board, coordinates, snake.head.coordinates, snake);
        if (!pathToEnemySegment.length) {
          return;
        }
        // 2nd segment (from 0) of 8 length snake, path is 2
        // on attack it will be 4th segment, damage is 4 (8 - 4)
        const enemyDamage = Math.max(enemySnake.length - (segmentIdx + pathToEnemySegment.length), 0);
        if (enemyDamage > 0 && (!bestPathToEnemySegment.length || pathToEnemySegment.length < bestPathToEnemySegment.length)) {
          bestPathToEnemySegment = pathToEnemySegment;
          // currentBiggestDamage = enemyDamage;
        }
      });
    }
    return bestPathToEnemySegment;
  }

  private getEnemyPaths(snake: Snake, enemyStrategy: AbstractStrategy): SnakePath[] {
    const enemies: Snake[] = snake.isPlayer ? this.board.getEnemies() : [this.board.getPlayer()];
    return enemies.map((enemy) => ({
      snake: enemy,
      path: enemyStrategy.evaluateBoard(enemy, enemy.isEnemy), // avoid cycling if pass player snake
    }));
  }
}
