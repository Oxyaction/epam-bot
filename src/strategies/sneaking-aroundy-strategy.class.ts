import { AbstractStrategy } from "./abstract-strategy.class";
import { Coordinates, Board, BoardObject } from "../board.class";
import { Command } from "../constants";
import { Snake } from "../snake.class";

export class SneakingAroundStrategy extends AbstractStrategy {
  public get name(): string {
    return 'sneaking-around';
  }

  public evaluateBoard(snake: Snake): Coordinates[] {
    const movableCoordinates: BoardObject[] =
      this.board.getNearMovable(snake.head.coordinates, this.pathfinder.buildMovableFilter(this.board, snake, 1));
    if (!movableCoordinates.length) {
      return [];
    }
    let coordinates: Coordinates;
    const snakeDirection: Command = snake.direction;
    const sameDirectionCoord
      = movableCoordinates.find(({ coordinates }) => Board.coordinatesToCommand(coordinates, snake.head.coordinates) === snakeDirection);
    // set same direction as default
    if (sameDirectionCoord) {
      coordinates = sameDirectionCoord.coordinates;
    } else {
      coordinates = movableCoordinates[0].coordinates;
    }
    let i = 0;

    while (!this.isSafeCoordinates(this.board, snake, coordinates) && i < movableCoordinates.length) {
      coordinates = movableCoordinates[i].coordinates;
      i++;
    }

    return coordinates ? [coordinates] : [movableCoordinates[0].coordinates];
  }

  /**
   * Checks that strongest enemy in adjacement coordinates
   * @param board
   * @param snake
   * @param coordinates
   */
  private isSafeCoordinates(board: Board, snake: Snake, coordinates: Coordinates): boolean {
    const enemies: Snake[] = board.getEnemies();
    const adjacementCellsWithEnemies: BoardObject[] =
      this.board.getNearMovable(coordinates, (boardObject) => {
        return enemies.some((enemy) => {
          return Board.coordinatesEqual(enemy.head.coordinates, boardObject.coordinates) && !snake.isStrongerThen(enemy, 1);
        })
      });
    return adjacementCellsWithEnemies.length === 0;
  }
}
