import { Element } from "../constants";
import { Snake } from "../snake.class";
import { Board, Coordinates, BoardObject } from "../board.class";
import { Pathfinder } from "../pathfinder.class";

export abstract class AbstractStrategy {
  constructor(protected readonly board: Board, protected readonly pathfinder: Pathfinder) { }

  public abstract evaluateBoard(snake: Snake, checkPathSafey?: boolean): Coordinates[];

  public abstract get name(): string;
}
