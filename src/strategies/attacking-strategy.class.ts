import { AbstractStrategy } from "./abstract-strategy.class";
import { Snake } from "../snake.class";
import { Enemy, Player } from "../constants";
import { Coordinates, BoardObject, MovableFilter, Board } from "../board.class";

export class AttackingStrategy extends AbstractStrategy {

  public get name(): string {
    return 'attacking';
  }

  public evaluateBoard(snake: Snake): Coordinates[] {
    let targets: BoardObject[] = this.board.getNearMovable(snake.head.coordinates, this.movableFilter(this.board, snake));
    targets = targets.sort(this.sortTargetsByPriority(this.board, snake));

    if (!targets.length) {
      return [];
    }
    return [targets[0].coordinates];
  }

  private movableFilter(board: Board, snake: Snake): MovableFilter {
    return function (boardObject: BoardObject): boolean {
      if (!Object.values(snake.isPlayer ? Enemy : Player).includes(boardObject.element)) {
        return false;
      }
      const enemy: Snake = snake.isPlayer ? board.getEnemySnakeByCoordinate(boardObject.coordinates) : board.getPlayer();
      if (!enemy) {
        return false;
      }

      if (snake.isStrongerThen(enemy, 1)) {
        return snake.isFury || Board.coordinatesEqual(boardObject.coordinates, enemy.head.coordinates); // bit tail or neck
      }
      return false;
    }
  }

  // killing should be priority than tail biting
  private sortTargetsByPriority(board: Board, snake: Snake) {
    return function (a: BoardObject, b: BoardObject): number {
      let aSnake = board.getEnemySnakeByCoordinate(a.coordinates);
      let bSnake = board.getEnemySnakeByCoordinate(b.coordinates);
      if (!aSnake || !aSnake.head) {
        return 1;
      } else if (Board.coordinatesEqual(a.coordinates, aSnake.head.coordinates)) {
        return -1;
      }
      if (!bSnake || !bSnake.head) {
        return 1;
      } else if (Board.coordinatesEqual(b.coordinates, bSnake.head.coordinates)) {
        return 1;
      }
      return 0;
    }
  }
}
