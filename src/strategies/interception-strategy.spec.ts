import { SnakeState } from "../snake.state.class";
import { Board, Coordinates } from "../board.class";
import { Snake } from "../snake.class";
import { InterceptionStrategy } from "./interception-strategy.class";
import { Pathfinder } from "../pathfinder.class";
import { log } from "util";

describe('InterceptionStrategy', () => {
  const getStrategyResolution = (boardAsString: string, state?: SnakeState): Coordinates[] => {
    const board = new Board(boardAsString);
    const player: Snake = board.getPlayer();
    if (state) {
      player.setState(state);
    } else {
      player.setState(new SnakeState());
    }

    const strategy = new InterceptionStrategy(board, new Pathfinder());
    return strategy.evaluateBoard(player);
  }

  describe('should attack', () => {
    it('when fury enough', () => {
      const board =
        '☼☼☼☼☼☼☼☼' +
        '☼     ○☼' +
        '☼ ╘═♥  ☼' +
        '☼      ☼' +
        '☼     ˄☼' +
        '☼     │☼' +
        '☼×────┘☼' +
        '☼☼☼☼☼☼☼☼';
      const state: SnakeState = new SnakeState();
      state.furyDuration = 2;
      const path: Coordinates[] = getStrategyResolution(board, state);
      expect(path).toEqual([{ x: 5, y: 2 }, { x: 6, y: 2 }]);
    });

    it('if bigger than enemy', () => {
      const board =
        '☼☼☼☼☼☼☼☼' +
        '☼     ○☼' +
        '☼ ╔═►  ☼' +
        '☼ ║    ☼' +
        '☼ ║   ˄☼' +
        '☼ ╙   ¤☼' +
        '☼      ☼' +
        '☼☼☼☼☼☼☼☼';
      const path: Coordinates[] = getStrategyResolution(board);
      expect(path).toEqual([{ x: 5, y: 2 }, { x: 6, y: 2 }]);
    });


    it('enemy in neck', () => {
      const board = "☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼●                          ☼☼#                           ☼☼☼  ○    ●           ╓       ☼☼☼                   ║  ○    ☼☼☼ ○                 ║       ☼☼☼     ☼☼☼☼☼         ╚═►     ☼☼☼     ☼                     ☼☼#     ☼☼☼        ☼☼☼☼#      ☼☼☼     ☼          ☼   ☼<┐●   ☼☼☼     ☼☼☼☼#æ     ☼☼☼☼# ¤    ☼☼☼          │     ☼          ☼☼☼○         ˅     ☼          ☼☼☼    ●  ®                   ☼☼#                           ☼☼☼                           ☼☼☼        ☼☼☼                ☼☼☼   ○   ☼  ☼                ☼☼☼      ☼☼☼☼#     ☼☼   ☼#    ☼☼☼      ☼   ☼   ● ☼ ☼ ☼ ☼ ○  ☼☼#      ☼   ☼     ☼  ☼  ☼    ☼☼☼                ☼     ☼    ☼☼☼     ●          ☼     ☼    ☼☼☼                           ☼☼☼                  ○        ☼☼☼ ○    ○   ○  ●         ○   ☼☼#                           ☼☼☼               ○      ○    ☼☼☼                           ☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼"
      const path: Coordinates[] = getStrategyResolution(board);
      expect(path).toEqual([{ x: 23, y: 7 }, { x: 23, y: 8 }]);
    });

    it('enemy tail when in rage', () => {
      const board =
        '☼☼☼☼☼☼☼☼' +
        '☼     ○☼' +
        '☼   ╘═♥☼' +
        '☼      ☼' +
        '☼×────┐☼' +
        '☼     │☼' +
        '☼     ˅☼' +
        '☼☼☼☼☼☼☼☼';
      const state: SnakeState = new SnakeState();
      state.furyDuration = 5;
      const path: Coordinates[] = getStrategyResolution(board, state);
      expect(path).toEqual([{ x: 6, y: 3 }, { x: 6, y: 4 }]);
    });
  });

  describe('should not attack', () => {
    it('if fury not enough', () => {
      const board =
        '☼☼☼☼☼☼☼☼' +
        '☼     ○☼' +
        '☼ ╘═♥  ☼' +
        '☼      ☼' +
        '☼     ˄☼' +
        '☼     │☼' +
        '☼×────┘☼' +
        '☼☼☼☼☼☼☼☼';
      const state: SnakeState = new SnakeState();
      state.furyDuration = 1;
      const path: Coordinates[] = getStrategyResolution(board, state);
      expect(path).toHaveLength(0);
    });

    it('if dies if attack', () => {
      const board =
        '☼☼☼☼☼☼☼☼' +
        '☼     ○☼' +
        '☼ ╘═►  ☼' +
        '☼      ☼' +
        '☼     ˄☼' +
        '☼     │☼' +
        '☼×────┘☼' +
        '☼☼☼☼☼☼☼☼';
      const path: Coordinates[] = getStrategyResolution(board);
      expect(path).toHaveLength(0);
    });

    it('should not return following path for stronger enemy', () => {
      const board =
        '☼☼☼☼☼☼☼☼' +
        '☼      ☼' +
        '☼ ╘═►  ☼' +
        '☼      ☼' +
        '☼     ˄☼' +
        '☼     │☼' +
        '☼×────┘☼' +
        '☼☼☼☼☼☼☼☼';
      const path: Coordinates[] = getStrategyResolution(board);
      expect(path).toHaveLength(0);
    });
  });
});
