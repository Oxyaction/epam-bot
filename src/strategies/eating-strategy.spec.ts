import { EatingStrategy } from './eating-strategy.class';
import { Board, Coordinates, BoardObject } from '../board.class';
import { Snake } from '../snake.class';
import { Pathfinder } from '../pathfinder.class';
import { SnakeState } from '../snake.state.class';

describe('EatingStrategy', () => {

  const getStrategyResolution = (boardAsString: string, state?: SnakeState): { board: Board, path: Coordinates[], player } => {
    const board = new Board(boardAsString);
    const player: Snake = board.getPlayer();
    if (state) {
      player.setState(state);
    } else {
      player.setState(new SnakeState());
    }
    const strategy = new EatingStrategy(board, new Pathfinder());
    const path = strategy.evaluateBoard(player);
    return {
      board,
      path,
      player,
      // target: {
      //   element: board.getElementByCoordinates(path[path.length - 1]),
      //   coordinates: path[path.length - 1],
      // },
    }
  }

  it('should target an apple', () => {
    const boardAsString =
      '☼☼☼☼☼☼' +
      '☼ ╘► ☼' +
      '☼    ☼' +
      '☼ ☼○☼☼' +
      '☼    ☼' +
      '☼☼☼☼☼☼';
    const { path }: { path: Coordinates[] } = getStrategyResolution(boardAsString);
    // expect(target.element).toBe('○');
    expect(path).toEqual([{ x: 3, y: 2 }, { x: 3, y: 3 }]);
  });

  it('should not return path to dead locked elements', () => {
    const boardAsString =
      '☼☼☼☼☼☼' +
      '☼ ╘► ☼' +
      '☼    ☼' +
      '☼ ☼○☼☼' +
      '☼  ☼ ☼' +
      '☼☼☼☼☼☼';
    const { path }: { path: Coordinates[] } = getStrategyResolution(boardAsString);

    // expect(target.element).not.toBe('○')
    expect(path).not.toEqual([{ x: 3, y: 2 }, { x: 3, y: 3 }]);
  });

  it('should prioritize stone over apple if fury enough', () => {
    const boardAsString =
      '☼☼☼☼☼☼' +
      '☼ ╘♥ ☼' +
      '☼    ☼' +
      '☼●  ○☼' +
      '☼    ☼' +
      '☼☼☼☼☼☼';
    const state: SnakeState = new SnakeState();
    state.furyDuration = 4;
    const { board, path } = getStrategyResolution(boardAsString, state);
    expect(board.getElementByCoordinates(path[path.length - 1])).toBe('●')
  });

  it('should not prioritize stone over apple if fury not enough', () => {
    const boardAsString =
      '☼☼☼☼☼☼' +
      '☼ ╘♥ ☼' +
      '☼    ☼' +
      '☼●  ○☼' +
      '☼    ☼' +
      '☼☼☼☼☼☼';
    const state: SnakeState = new SnakeState();
    state.furyDuration = 3;
    const { board, path } = getStrategyResolution(boardAsString, state);
    expect(board.getElementByCoordinates(path[path.length - 1])).toBe('○')
    // expect(target.element).toBe('○')
  });

  it('should not target food if enemy gets there faster', () => {
    const boardAsString = "☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼┌───────ö                  ☼☼#└─┐   ◄════════════╗       ☼☼☼  └♣○  ●           ╙       ☼☼☼                           ☼☼☼ ○         ●               ☼☼☼     ☼☼☼☼☼                 ☼☼☼ ●   ☼ ○                   ☼☼#     ☼☼☼        ☼☼☼☼#      ☼☼☼     ☼          ☼   ☼  ●   ☼☼☼     ☼☼☼☼#      ☼☼☼☼#    ● ☼☼☼                ☼          ☼☼☼○               ☼          ☼☼☼    ●  ©            ○      ☼☼#                           ☼☼☼                           ☼☼☼        ☼☼☼                ☼☼☼   ○   ☼  ☼                ☼☼☼      ☼☼☼☼#     ☼☼   ☼#    ☼☼☼      ☼  ®☼   ● ☼ ☼ ☼ ☼    ☼☼#      ☼   ☼     ☼  ☼  ☼    ☼☼☼                ☼     ☼    ☼☼☼     ●    $     ☼     ☼    ☼☼☼                           ☼☼☼            ○              ☼☼☼     ○       ●             ☼☼#                 ○         ☼☼☼                           ☼☼☼                           ☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼";
    const { path } = getStrategyResolution(boardAsString);
    expect(path).not.toEqual([{ x: 7, y: 2 }, { x: 6, y: 2 }, { x: 6, y: 3 }]);
  });

  it('should not go through enemy', () => {
    const boardAsString = '☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼                           ☼☼#                           ☼☼☼           $             ® ☼☼☼        ●           ●      ☼☼☼                           ☼☼☼     ☼☼☼☼☼            ○    ☼☼☼     ☼              ○      ☼☼#     ☼☼☼        ☼☼☼☼#      ☼☼☼     ☼     ○    ☼  ○☼      ☼☼☼     ☼☼☼☼#      ☼☼☼☼#      ☼☼☼                ☼          ☼☼☼             ©  ☼          ☼☼☼                           ☼☼#                           ☼☼☼                          ○☼☼☼        ☼☼☼                ☼☼☼       ☼  ☼                ☼☼☼      ☼☼☼☼#  ●  ☼☼   ☼#    ☼☼☼      ☼   ☼     ☼ ☼ ☼ ☼○   ☼☼#      ☼   ☼     ☼  ☼  ☼    ☼☼☼            æ   ☼     ☼    ☼☼☼            │   ☼     ☼    ☼☼☼            │              ☼☼☼╓           │              ☼☼☼║           │              ☼☼#║           └─┐       ●    ☼☼☼║             └─┐         ○☼☼☼╚════════════►  └────>  ○  ☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼';
    const { path } = getStrategyResolution(boardAsString);
    expect(path[0]).not.toEqual({ x: 16, y: 28 });
  });

  it('should prioritize fury pill if have stone and snake length not too big', () => {
    const boardAsString =
      '☼☼☼☼☼☼' +
      '☼ ╘♥ ☼' +
      '☼    ☼' +
      '☼●  ○☼' +
      '☼    ☼' +
      '☼☼☼☼☼☼';
    const state: SnakeState = new SnakeState();
    state.furyDuration = 3;
    const { board, path } = getStrategyResolution(boardAsString, state);
    expect(board.getElementByCoordinates(path[path.length - 1])).toBe('○')
    // expect(target.element).toBe('○')
  });

  it('should avoid tackles', () => {
    const board =
      '☼☼☼☼☼☼☼☼' +
      '☼   ╓  ☼' +
      '☼   ▼  ☼' +
      '☼    ˄○☼' +
      '☼   ○│ ☼' +
      '☼    │ ☼' +
      '☼×───┘ ☼' +
      '☼☼☼☼☼☼☼☼';
    const { path } = getStrategyResolution(board);
    expect(path).toHaveLength(0);
  });

  it('should not go to dangerous cell #1', () => {
    const boardAsString = '☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼' +
      '☼☼ ○                         ☼' +
      '☼#                    ●      ☼' +
      '☼☼●      ●                   ☼' +
      '☼☼                           ☼' +
      '☼☼       ●   ●               ☼' +
      '☼☼     ☼☼☼☼☼                 ☼' +
      '☼☼     ☼                     ☼' +
      '☼#     ☼☼☼        ☼☼☼☼#     ○☼' +
      '☼☼     ☼$         ☼   ☼  ●   ☼' +
      '☼☼     ☼☼☼☼#      ☼☼☼☼#      ☼' +
      '☼☼                ☼          ☼' +
      '☼☼                ☼          ☼' +
      '☼☼    ●                 ○    ☼' +
      '☼#                  ┌─>      ☼' +
      '☼☼                  └┐       ☼' +
      '☼☼        ☼☼☼    ○   │╔►     ☼' +
      '☼☼       ☼  ☼    ┌───┘╚══╗   ☼' +
      '☼☼      ☼☼☼☼#    │☼☼   ☼#╚╗  ☼' +
      '☼☼      ☼   ☼    │☼ ☼ ☼ ☼ ║  ☼' +
      '☼#      ☼   ☼    │☼  ☼  ☼╔╝  ☼' +
      '☼☼               │☼     ☼╚╕  ☼' +
      '☼☼     ●         │☼     ☼    ☼' +
      '☼☼               ¤           ☼' +
      '☼☼                           ☼' +
      '☼☼             ●             ☼' +
      '☼#                           ☼' +
      '☼☼                           ☼' +
      '☼☼ $                 ○       ☼' +
      '☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼';
    const { path } = getStrategyResolution(boardAsString);
    expect(path[0]).not.toEqual({ x: 23, y: 15 });
  });

  it('should not go to dangerous cell #2', () => {
    const boarAsString = '☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼' +
      '☼☼     ○             ○       ☼' +
      '☼#                           ☼' +
      '☼☼                          ○☼' +
      '☼☼     ┌──────ö       ○      ☼' +
      '☼☼   ♣─┘ ©   ●     ○ ○       ☼' +
      '☼☼    ®☼☼☼☼☼         ○    ○  ☼' +
      '☼☼    ▲                      ☼' +
      '☼#  ╔═╝☼☼☼       ○☼☼☼☼#      ☼' +
      '☼☼  ║  ☼ ○        ☼   ☼  ●   ☼' +
      '☼☼  ║  ☼☼☼☼#      ☼☼☼☼#      ☼' +
      '☼☼  ║             ☼          ☼' +
      '☼☼  ╚╗   ●        ☼          ☼' +
      '☼☼   ║●                      ☼' +
      '☼#  ○║                   ○   ☼' +
      '☼☼   ╙                       ☼' +
      '☼☼        ☼☼☼                ☼' +
      '☼☼       ☼  ☼                ☼' +
      '☼☼      ☼☼☼☼#     ☼☼   ☼#    ☼' +
      '☼☼      ☼ ○ ☼     ☼ ☼ ☼ ☼    ☼' +
      '☼#      ☼   ☼     ☼  ☼  ☼    ☼' +
      '☼☼         ●      ☼     ☼    ☼' +
      '☼☼     ●          ☼     ☼    ☼' +
      '☼☼                        ○  ☼' +
      '☼☼                           ☼' +
      '☼☼  ○                     ○  ☼' +
      '☼#          ©                ☼' +
      '☼☼                           ☼' +
      '☼☼                           ☼' +
      '☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼';

    const { path } = getStrategyResolution(boarAsString);
    expect(path[0]).not.toEqual({ x: 5, y: 7 });
  });

  it.skip('should not go to dangerous cell #3', () => {
    const boardAsString = "☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼" +
      "☼☼                     ○     ☼" +
      "☼#                          ○☼" +
      "☼☼                           ☼" +
      "☼☼                     ○     ☼" +
      "☼☼           ●  ○            ☼" +
      "☼☼     ☼☼☼☼☼         ○    ○ ●☼" +
      "☼☼     ☼                 ○   ☼" +
      "☼#     ☼☼☼        ☼☼☼☼#      ☼" +
      "☼☼     ☼          ☼   ☼  ●  ○☼" +
      "☼☼●    ☼☼☼☼#      ☼☼☼☼#      ☼" +
      "☼☼     ▲          ☼          ☼" +
      "☼☼  ╔═╕║        ○ ☼          ☼" +
      "☼☼  ║╔═╝  <──┐        ●      ☼" +
      "☼#  ║║       └─────┐         ☼" +
      "☼☼  ╚╝             │     ○   ☼" +
      "☼☼        ☼☼☼      │┌────┐   ☼" +
      "☼☼       ☼  ☼      └┘    ¤   ☼" +
      "☼☼      ☼☼☼☼#     ☼☼   ☼#    ☼" +
      "☼☼      ☼   ☼   ● ☼ ☼ ☼ ☼ ○  ☼" +
      "☼#      ☼   ☼     ☼  ☼ ○☼    ☼" +
      "☼☼         ● ©    ☼     ☼    ☼" +
      "☼☼                ☼     ☼    ☼" +
      "☼☼                           ☼" +
      "☼☼                           ☼" +
      "☼☼             ●   ©     ○   ☼" +
      "☼#                           ☼" +
      "☼☼                          ○☼" +
      "☼☼                           ☼" +
      "☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼";

    const { board, path } = getStrategyResolution(boardAsString);
    console.log(board.getBoardAsDrawableString());
    expect(path[0]).not.toEqual({ x: 8, y: 11 });

  });
});
