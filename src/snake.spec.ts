import { Snake } from './snake.class';
import { Board } from './board.class';

describe('snake', () => {
  describe('moveToPath', () => {
    const boardAsString =
      '☼☼☼☼☼☼☼☼' +
      '☼╓     ☼' +
      '☼║     ☼' +
      '☼╚═►   ☼' +
      '☼      ☼' +
      '☼      ☼' +
      '☼      ☼' +
      '☼☼☼☼☼☼☼☼';

    it('should correctly change coordinates on board and in snake if path same as snake length', () => {
      const board: Board = new Board(boardAsString);
      const snake: Snake = board.getPlayer();
      const path = [{ x: 4, y: 3 }, { x: 5, y: 3 }, { x: 6, y: 3 }, { x: 6, y: 4 }, { x: 6, y: 5 }];
      snake.moveToPath(board, path);
      expect(snake.coordinates).toEqual(path.reverse());
      const expectedBoard =
        '☼☼☼☼☼☼☼☼' +
        '☼      ☼' +
        '☼      ☼' +
        '☼   ╘═╗☼' +
        '☼     ║☼' +
        '☼     ▼☼' +
        '☼      ☼' +
        '☼☼☼☼☼☼☼☼';
      expect(board.getBoardAsString()).toBe(expectedBoard);
    });

    it('should correctly change coordinates on board and in snake if path is more than snake length', () => {
      const board: Board = new Board(boardAsString);
      const snake: Snake = board.getPlayer();
      const path = [{ x: 4, y: 3 }, { x: 5, y: 3 }, { x: 6, y: 3 }, { x: 6, y: 4 }, { x: 6, y: 5 }, { x: 6, y: 6 }];
      snake.moveToPath(board, path);
      expect(snake.coordinates).toEqual([{ "x": 6, "y": 6 }, { "x": 6, "y": 5 }, { "x": 6, "y": 4 }, { "x": 6, "y": 3 }, { "x": 5, "y": 3 }]);
      const expectedBoard =
        '☼☼☼☼☼☼☼☼' +
        '☼      ☼' +
        '☼      ☼' +
        '☼    ╘╗☼' +
        '☼     ║☼' +
        '☼     ║☼' +
        '☼     ▼☼' +
        '☼☼☼☼☼☼☼☼';
      expect(board.getBoardAsString()).toBe(expectedBoard);
    });

    it('should correctly change coordinates on board and in snake if path less than snake length', () => {
      const board: Board = new Board(boardAsString);
      const snake: Snake = board.getPlayer();
      const path = [{ x: 4, y: 3 }];
      snake.moveToPath(board, path);
      expect(snake.coordinates).toEqual([{ x: 4, y: 3 }, { x: 3, y: 3 }, { x: 2, y: 3 }, { x: 1, y: 3 }, { x: 1, y: 2 }]);
      const expectedBoard =
        '☼☼☼☼☼☼☼☼' +
        '☼      ☼' +
        '☼╓     ☼' +
        '☼╚══►  ☼' +
        '☼      ☼' +
        '☼      ☼' +
        '☼      ☼' +
        '☼☼☼☼☼☼☼☼';
      expect(board.getBoardAsString()).toBe(expectedBoard);
    });
  });
});
