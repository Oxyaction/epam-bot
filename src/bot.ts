/*-
 * #%L
 * Codenjoy - it's a dojo-like platform from developers to developers.
 * %%
 * Copyright (C) 2018 - 2019 Codenjoy
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
import { ELEMENT, COMMANDS, Command } from './constants';
import {
    isGameOver, getHeadPosition, getElementByXY, findPathToCoords, coordsToCommand, isMovableElement,
    getNearMovable, getNextToHeadCoords,
    isOutOf,
    isCoordNear,
    setElementByXY,
    getBoardSize,
    coordsEqual,
    commandToCoords,
    sendResetRequest
} from './utils';
import {
    Board, Coordinates,
} from './board.class';
import { Snake } from './snake.class';
import { SnakeState } from './snake.state.class';
import { Pathfinder } from './pathfinder.class';
import { Logger } from './logger/logger.class';
import * as config from 'config';
import { DecisionManager } from './strategies/decision-manager.class';

const STATE_DURATION = 10;
const IDLING_THRESHOLD = 100;

// let searchRatio = 1;
let snakeLength = 2;
let snakeState = null;
let stateDuration = 0;
let previousMove = null;
let previousElement = null;
let iterationsIdling = 0;

let gameTick = -1;

export function getSnakeMove2(_board: string, _snakeState: SnakeState, logger?: Logger): string {
    gameTick++;
    const debug = config.get('debug');
    const debugMessages = [];
    debugMessages.push('==== NEW TICK ====');
    debugMessages.push(`tick ${gameTick}`);
    const board: Board = new Board(_board);
    const snake: Snake = board.getPlayer();
    snake.setState(_snakeState);
    let commandAsString: string
    if (snake.isActive) {
        const command: Command[] = [];
        const start = Date.now();

        if (gameTick === 0) {
            debugMessages.push('Game started');
            onGameStarted(logger);
        }

        const decisionManager = new DecisionManager(board, new Pathfinder(logger), debugMessages, logger);
        const { path, act }: { path: Coordinates[], act: boolean } = decisionManager.decide(snake);
        command.push(Board.coordinatesToCommand(path[0], snake.head.coordinates));
        if (act) {
            command.push(Command.ACT)
        }
        commandAsString = command.join(',');
        const target = board.getElementByCoordinates(path[path.length - 1]);
        debugMessages.push("Next target is " + JSON.stringify(target));
        debugMessages.push("Path is " + JSON.stringify(path));

        if (logger) {
            logger.log({
                tick: gameTick,
                board: board.getBoardAsString(),
                path: path,
                debug: {
                    command: commandAsString,
                    target,
                    currentPosition: snake.head.coordinates,
                    snakeState: snake.getState().getState()
                },
            });
        }
        snake.getState().update(board, command[0], path[0]);
        debugMessages.push('TOTAL SPENT FOR MOVE ' + (Date.now() - start) + 'ms');
    } else {
        if (board.isPlayerSleeping()) {
            onGameIdling(logger, _snakeState, debugMessages);
        } else if (board.isPlayerDead()) {
            if (logger) {
                logger.log({
                    tick: gameTick,
                    board: board.getBoardAsString(),
                    path: [],
                    debug: {
                        snakeState: snake.getState().getState()
                    },
                });
            }
            gameTick < 300 && onDeath(logger, debugMessages);
        }
        commandAsString = Command.RIGHT;
    }

    debug && console.log(debugMessages.join("\n"));

    return commandAsString;
}

function onGameStarted(logger: Logger) {
    // logger inited
    logger && logger.init();
}

function onGameIdling(logger: Logger, _snakeState: SnakeState, debugMessages: string[]): void {
    if (gameTick > 0 && logger && config.get('flushLogsOn') === 'always') {
        // log previous game
        logger.flush();
    }
    gameTick = -1;
    _snakeState.reset();
    debugMessages.push('Waiting for game start');
}

function onDeath(logger: Logger, debugMessages: string[]): void {
    if (logger) {
        logger.setDead();
        config.get('flushLogsOn') === 'death' && logger.flush();
    }
    debugMessages.push('Snake is dead');
}

// Bot Example
export function getNextSnakeMove(board) {
    console.log('==== NEW MOVE =====');
    const start = Date.now();
    if (isGameOver(board)) {
        return '';
    }
    const headPosition = getHeadPosition(board);
    if (!headPosition) {
        return '';
    }

    snakeLength = countSnakeLength(board, headPosition);
    if (previousElement === ELEMENT.STONE) {
        snakeLength -= 3;
    }
    console.log('snake length', snakeLength);

    if (snakeState) {
        stateDuration -= 1;
        if (stateDuration <= 0) {
            snakeState = null;
            console.log('STATE FINISHED');
        }
    }

    const enemies = searchEnemies(board, headPosition);
    let path;
    let command = '';
    if (enemies.length) {
        path = interceptEnemyPath(board, enemies, headPosition, snakeLength, snakeState, stateDuration);
        if (path.length) {
            console.log('Attacking at: ' + JSON.stringify(path[path.length - 1]));
        }
    }
    if (!path || !path.length) {
        let sorround = getSorround(board, headPosition, 10, snakeLength, snakeState); // (LEFT, UP, RIGHT, DOWN)
        sorround.forEach(rateSurround);
        sorround = sortCoords(sorround, headPosition);
        const result = findOptimalPath(board, sorround, headPosition, snakeLength, 10, snakeState, stateDuration);
        console.log('Targeting: ' + JSON.stringify(result.surround));
        path = result.path;

        // if (![ELEMENT.STONE, ELEMENT.GOLD].includes(result.surround.element) && ((snakeState === ELEMENT.FURY_PILL && stateDuration > 4) || snakeLength - 3 >= 2)) {
        if ((snakeState === ELEMENT.FURY_PILL && stateDuration > 2) || (snakeLength - 3 >= 2 && previousElement !== ELEMENT.STONE && result.surround.element !== ELEMENT.STONE)) {
            command = ',ACT';
        }
    }

    let move = path[0];

    console.log('Priority path is: ' + JSON.stringify(path));
    console.log('Going to: ' + JSON.stringify(move));

    if (!move) {
        console.log('path not found, using alternative move');
        move = getAlternativeMove(board, headPosition);
    }
    // game restarted
    checkGameIdling(move);

    previousMove = move;
    previousElement = getElementByXY(board, move);

    checkStateChanges(board, move);

    console.log('TOTAL SPENT FOR MOVE ', Date.now() - start, 'ms');
    return coordsToCommand(move, headPosition) + command;
}

function checkGameIdling(move) {
    if (previousMove && coordsEqual(previousMove, move)) {
        console.log('GAME RESTARTED');
        iterationsIdling++;
        console.log('idling ', iterationsIdling, ' iterations');
        if (iterationsIdling > IDLING_THRESHOLD) {
            sendResetRequest();
            iterationsIdling = 0;
        }
        resetState();
    } else {
        iterationsIdling = 0;
    }
}

function checkStateChanges(board, move) {
    if (willConsumeOnNextStep(board, move, ELEMENT.FURY_PILL)) {
        snakeState = ELEMENT.FURY_PILL;
        stateDuration = STATE_DURATION;
    }
    if (willConsumeOnNextStep(board, move, ELEMENT.FLYING_PILL)) {
        snakeState = ELEMENT.FLYING_PILL;
        stateDuration = STATE_DURATION;
    }
    // if (willConsumeOnNextStep(board, move, ELEMENT.STONE)) {
    //     snakeLength -= 3;
    // }
}

export function getSorround(board, position, limit = 10, snakeLength = 2, state) {
    const p = position;
    const surround = [];
    const start = Date.now();
    // const limit = 5 * searchRatio;
    for (let i = -limit; i < limit + 1; i++) {
        for (let j = -limit; j < limit + 1; j++) {
            let coords = { x: p.x + i, y: p.y + j };
            if (isOutOf(board, coords.x, coords.y)) {
                continue;
            }
            let element = getElementByXY(board, coords);
            const nearMovable = getNearMovable(board, coords, snakeLength, state);
            // if element is good (apple, gold...) and have exit
            // isNear(board, position.x, position.y)
            if (isMovableElement(element, snakeLength, state) && (nearMovable.length > 1 || (isCoordNear(board, coords, position) && nearMovable.length > 0))) {
                surround.push({
                    coords,
                    element
                });
            }
        }
    }
    return surround;
}

export function rateSurround(surround) {
    switch (surround.element) {
        case ELEMENT.NONE:
            surround.value = 0;
            break;
        case ELEMENT.FLYING_PILL:
            surround.value = 1;
            break;
        case ELEMENT.APPLE:
            surround.value = 2;
            break;
        case ELEMENT.FURY_PILL:
            surround.value = 3;
            break;
        case ELEMENT.STONE:
        case ELEMENT.GOLD:
            surround.value = 4;
            break;
        default:
            surround.value = -1;
    }
}


export function sortCoords(raitings, currentPosition) {
    return raitings.sort((a, b) => {
        if (a.value > b.value) {
            return -1
        } else if (a.value < b.value) {
            return 1;
        } else if (coordsToCommand(a.coords, currentPosition)) {
            return -1
        } else if (coordsToCommand(b.coords, currentPosition)) {
            return 1;
        }
        return 0;
    });
}

export function findOptimalPath(board, surroundings, headPosition, snakeLength, numberOfTargets = 5, state, stateDuraton, disableSafe = false) {
    let chosenSurround = surroundings[0];
    let minPath = [];
    let minDistance = Infinity;
    surroundings.slice(0, numberOfTargets).forEach((surround) => {
        const path = findPathToCoords(board, surround.coords, headPosition, snakeLength, state);
        if (
            path.length > 0
            && (path.length < minDistance && surround.value > 1)
            && (disableSafe || isSafePath(board, path, snakeLength, state, stateDuraton))
        ) {
            chosenSurround = surround;
            minDistance = path.length;
            minPath = path;
        }
    });

    return { path: minPath, surround: chosenSurround };
}

export function isSafePath(board, path, length, state, duration = 0) {
    let newBoard = board;
    path.forEach((position) => {
        newBoard = setElementByXY(board, position, ELEMENT.WALL);
    });
    const boardSize = getBoardSize(newBoard);
    const target = { x: boardSize / 2, y: boardSize / 2 };
    newBoard = setElementByXY(board, target, ELEMENT.NONE);
    const escapePath = findPathToCoords(newBoard, target, path[path.length - 1], snakeState, null);
    if (!escapePath.length) {
        return false;
    }

    const closestEnemies = searchEnemies(board, path[path.length - 1], true);

    for (let i = 0; i < closestEnemies.length; i++) {
        const enemyPath = findPathToCoords(newBoard, path[path.length - 1], closestEnemies[i], snakeState, null);
        // probably will tackle
        if (enemyPath.length === path.length) {
            const enemyHead = getElementByXY(board, closestEnemies[i]);
            if (enemyHead === ELEMENT.ENEMY_HEAD_EVIL) {
                return false;
            } else if (state === ELEMENT.FURY_PILL && duration >= path.length) {
                return true;
            } else if (length - getEnemySnakeLength(board, closestEnemies[i]) < 2) {
                return false;
            }
        }
    }

    return true;
}

export function getAlternativeMove(board, headPosition) {
    const nextCoords = getNextToHeadCoords(board, headPosition);
    if (isMovableElement(getElementByXY(board, nextCoords), snakeLength, snakeState)
        && getNearMovable(board, nextCoords, snakeLength, snakeState).length > 1) {
        return nextCoords;
    }
    const alternativeDirections = {
        [ELEMENT.HEAD_DOWN]: [
            { x: headPosition.x - 1, y: headPosition.y },
            { x: headPosition.x + 1, y: headPosition.y }
        ],
        [ELEMENT.HEAD_UP]: [
            { x: headPosition.x - 1, y: headPosition.y },
            { x: headPosition.x + 1, y: headPosition.y }
        ],
        [ELEMENT.HEAD_LEFT]: [
            { x: headPosition.x, y: headPosition.y - 1 },
            { x: headPosition.x, y: headPosition.y + 1 }
        ],
        [ELEMENT.HEAD_RIGHT]: [
            { x: headPosition.x, y: headPosition.y - 1 },
            { x: headPosition.x, y: headPosition.y + 1 }
        ]
    };

    const head = getElementByXY(board, headPosition);
    if (!alternativeDirections[head]) {
        return { x: headPosition.x + 1, y: headPosition.y };
    }
    for (let i = 0; i < alternativeDirections[head].length; i++) {
        if (isMovableElement(getElementByXY(board, alternativeDirections[head][i]), snakeLength, snakeState)
            && getNearMovable(board, alternativeDirections[head][i], snakeLength, snakeState).length > 1) {
            return alternativeDirections[head][i];
        }
    }
    return nextCoords;
}

export function willConsumeOnNextStep(board, move, element) {
    return element === getElementByXY(board, move);
}

function resetState() {
    snakeState = null;
    stateDuration = 0;
}


export function searchEnemies(board, headPosition, withRage = false) {
    const enemyHead = [
        ELEMENT.ENEMY_HEAD_DOWN,
        ELEMENT.ENEMY_HEAD_LEFT,
        ELEMENT.ENEMY_HEAD_RIGHT,
        ELEMENT.ENEMY_HEAD_UP,
        // ELEMENT.ENEMY_HEAD_DEAD,
        // ELEMENT.ENEMY_HEAD_EVIL,
        // ELEMENT.ENEMY_HEAD_FLY,
        // ELEMENT.ENEMY_HEAD_SLEEP
    ];
    if (withRage) {
        enemyHead.push(ELEMENT.ENEMY_HEAD_EVIL);
    }
    const range = 5;
    const enemies = [];
    for (let i = -range; i < range + 1; i++) {
        for (let j = -range; j < range + 1; j++) {
            const coords = { x: headPosition.x + i, y: headPosition.y + j };
            const element = getElementByXY(board, coords);
            if (enemyHead.includes(element)) {
                // !TODO ADD CHECK SNAKE DOESNT DIE
                enemies.push(coords);
            }
        }
    }
    return enemies;
}

export function interceptEnemyPath(board, enemies, headCoords, snakeLength, state, stateDuration) {
    for (let i = 0; i < enemies.length; i++) {
        const enemyHeadCoords = enemies[i];
        if (snakeLength - getEnemySnakeLength(board, enemyHeadCoords) >= 2 || state === ELEMENT.FURY_PILL) { // attack
            // Get enemy path
            let surroundings = getSorround(board, enemyHeadCoords, 5, 0, null);
            surroundings.forEach(rateSurround);
            surroundings = sortCoords(surroundings, enemyHeadCoords);

            const { path: enemyPredictablePath } = findOptimalPath(board, surroundings, enemyHeadCoords, 0, 3, null, 0, true);
            if (!enemyPredictablePath) {
                continue;
            }

            // Try to intercept
            for (let j = 0; j < enemyPredictablePath.length; j++) {
                const enemyPathCoord = enemyPredictablePath[j];
                const interceptionPath = findPathToCoords(board, enemyPathCoord, headCoords, 0, state);
                // attack matches
                if (
                    interceptionPath.length - 1 === j
                    && (
                        snakeLength - getEnemySnakeLength(board, enemyHeadCoords) >= 2
                        || (state === ELEMENT.FURY_PILL && stateDuration >= interceptionPath.length)
                    )
                ) {
                    return interceptionPath;
                }
            }
        }
    }
    return [];
}

export function getEnemySnakeLength(board, coords) {
    const element = getElementByXY(board, coords);
    const enemyTails = [
        ELEMENT.ENEMY_TAIL_END_DOWN,
        ELEMENT.ENEMY_TAIL_END_LEFT,
        ELEMENT.ENEMY_TAIL_END_RIGHT,
        ELEMENT.ENEMY_TAIL_END_UP
    ];

    const elementToCommand = {
        [ELEMENT.ENEMY_BODY_HORIZONTAL]: from => from === COMMANDS.RIGHT ? COMMANDS.RIGHT : COMMANDS.LEFT,
        [ELEMENT.ENEMY_BODY_VERTICAL]: from => from === COMMANDS.DOWN ? COMMANDS.DOWN : COMMANDS.UP,

        [ELEMENT.ENEMY_BODY_LEFT_DOWN]: from => from === COMMANDS.RIGHT ? COMMANDS.DOWN : COMMANDS.LEFT,
        [ELEMENT.ENEMY_BODY_LEFT_UP]: from => from === COMMANDS.RIGHT ? COMMANDS.UP : COMMANDS.LEFT,

        [ELEMENT.ENEMY_BODY_RIGHT_DOWN]: from => from === COMMANDS.LEFT ? COMMANDS.DOWN : COMMANDS.RIGHT,
        [ELEMENT.ENEMY_BODY_RIGHT_UP]: from => from === COMMANDS.LEFT ? COMMANDS.UP : COMMANDS.RIGHT,

        [ELEMENT.ENEMY_HEAD_DOWN]: () => COMMANDS.UP,
        [ELEMENT.ENEMY_HEAD_UP]: () => COMMANDS.DOWN,
        [ELEMENT.ENEMY_HEAD_RIGHT]: () => COMMANDS.LEFT,
        [ELEMENT.ENEMY_HEAD_LEFT]: () => COMMANDS.RIGHT,
    }

    let enemySnakeLength = 1;
    let currentElement = element;
    let prevCommand = null;
    let currentCoords = coords;

    while (!enemyTails.includes(currentElement)) {
        enemySnakeLength++;
        if (!elementToCommand[currentElement]) {
            console.error('Unknown element', currentElement);
            return 100;
        }
        const command = elementToCommand[currentElement](prevCommand);
        const newCords = commandToCoords(currentCoords, command);
        currentElement = getElementByXY(board, newCords);
        currentCoords = newCords;
        prevCommand = command;
    }

    return enemySnakeLength;
}

export function countSnakeLength(board, coords) {
    let length = 0;
    const userParts = [
        ELEMENT.HEAD_DOWN,
        ELEMENT.HEAD_LEFT,
        ELEMENT.HEAD_RIGHT,
        ELEMENT.HEAD_UP,
        ELEMENT.HEAD_DEAD,
        ELEMENT.HEAD_EVIL,
        ELEMENT.HEAD_FLY,
        ELEMENT.HEAD_SLEEP,

        ELEMENT.TAIL_END_DOWN,
        ELEMENT.TAIL_END_LEFT,
        ELEMENT.TAIL_END_UP,
        ELEMENT.TAIL_END_RIGHT,
        ELEMENT.TAIL_INACTIVE,

        ELEMENT.BODY_HORIZONTAL,
        ELEMENT.BODY_VERTICAL,
        ELEMENT.BODY_LEFT_DOWN,
        ELEMENT.BODY_LEFT_UP,
        ELEMENT.BODY_RIGHT_DOWN,
        ELEMENT.BODY_RIGHT_UP,
    ];
    for (var i = 0; i < board.length; i++) {
        var char = board[i];
        if (userParts.includes(char)) {
            length++;
        }
    }

    return length;
    // const element = getElementByXY(board, coords);
    // const tails = [
    //     ELEMENT.TAIL_END_DOWN,
    //     ELEMENT.TAIL_END_LEFT,
    //     ELEMENT.TAIL_END_RIGHT,
    //     ELEMENT.TAIL_END_UP
    // ];

    // const elementToCommand = {
    //     [ELEMENT.BODY_HORIZONTAL]: from => from === COMMANDS.RIGHT ? COMMANDS.RIGHT : COMMANDS.LEFT,
    //     [ELEMENT.BODY_VERTICAL]: from => from === COMMANDS.DOWN ? COMMANDS.DOWN : COMMANDS.UP,

    //     [ELEMENT.BODY_LEFT_DOWN]: from => from === COMMANDS.RIGHT ? COMMANDS.DOWN : COMMANDS.LEFT,
    //     [ELEMENT.BODY_LEFT_UP]: from => from === COMMANDS.RIGHT ? COMMANDS.UP : COMMANDS.LEFT,

    //     [ELEMENT.BODY_RIGHT_DOWN]: from => from === COMMANDS.LEFT ? COMMANDS.DOWN : COMMANDS.RIGHT,
    //     [ELEMENT.BODY_RIGHT_UP]: from => from === COMMANDS.LEFT ? COMMANDS.UP : COMMANDS.RIGHT,

    //     [ELEMENT.HEAD_DOWN]: () => COMMANDS.UP,
    //     [ELEMENT.HEAD_UP]: () => COMMANDS.DOWN,
    //     [ELEMENT.HEAD_RIGHT]: () => COMMANDS.LEFT,
    //     [ELEMENT.HEAD_LEFT]: () => COMMANDS.RIGHT,
    // }

    // let snakeLengthCounter = 1;
    // let currentElement = element;
    // let prevCommand = null;
    // let currentCoords = coords;

    // while (!tails.includes(currentElement)) {
    //     snakeLengthCounter++;
    //     const command = elementToCommand[currentElement](prevCommand);
    //     const newCords = commandToCoords(currentCoords, command);
    //     currentElement = getElementByXY(board, newCords);
    //     currentCoords = newCords;
    //     prevCommand = command;
    // }

    // return snakeLengthCounter;
}
