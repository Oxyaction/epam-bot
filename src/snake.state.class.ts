import { Element, Command, ELEMENT } from './constants';
import { Board, Coordinates } from './board.class';

export class SnakeState {
  private _furyDuration: number;
  public _flyingDuration: number;
  private _stonesCount: number;
  public previousElement: Element = Element.NONE;
  public previousCommand: Command = Command.RIGHT;

  constructor({ flyingDuration = 0, furyDuration = 0, stonesCount = 0 }: { flyingDuration?: number, furyDuration?: number, stonesCount?: number } = {}) {
    this.flyingDuration = flyingDuration;
    this.furyDuration = furyDuration;
    this._stonesCount = stonesCount;
  }

  public reset() {
    this._furyDuration = 0;
    this.previousElement = Element.NONE;
    this.previousCommand = Command.RIGHT;
  }

  public update(board: Board, command: Command, coordinates: Coordinates) {
    this.previousElement = board.getElementByCoordinates(coordinates);
    this.previousCommand = command;
    if (this.previousElement === Element.FURY_PILL) {
      this._furyDuration = 10;
    }
    if (this.previousElement === Element.STONE) {
      this._stonesCount += 1;
    }
    this._furyDuration = Math.max(0, this._furyDuration - 1);
    this._flyingDuration = Math.max(0, this._flyingDuration - 1);
  }

  public getState() {
    return {
      previousElement: this.previousElement,
      previousCommand: this.previousCommand,
      furyDuration: this._furyDuration,
      flyingDuration: this._flyingDuration,
      stonesCount: this._stonesCount,
    }
  }

  public incrementTimers(incrBy: number = 1): void {
    this._furyDuration += incrBy;
  }

  public copy(): SnakeState {
    const snakeState = new SnakeState();
    snakeState._furyDuration = this._furyDuration;
    snakeState._flyingDuration = this._flyingDuration;
    snakeState._stonesCount = this._stonesCount;
    snakeState.previousCommand = this.previousCommand;
    snakeState.previousElement = this.previousElement;
    return snakeState;
  }

  public get furyDuration(): number {
    return this._furyDuration;
  }

  public set furyDuration(value: number) {
    if (value > 10 || value < 0) {
      throw new Error('furyDuration value can be between 0 and 10');
    }
    this._furyDuration = value;
  }

  public get flyingDuration(): number {
    return this._flyingDuration;
  }

  public set flyingDuration(value: number) {
    if (value > 10 || value < 0) {
      throw new Error('flyingDuration value can be between 0 and 10');
    }
    this._flyingDuration = value;
  }

  public get stonesCount(): number {
    return this._stonesCount;
  }

  public decrementStone(): void {
    this._stonesCount -= 1;
  }
}
