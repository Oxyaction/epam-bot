/*-
 * #%L
 * Codenjoy - it's a dojo-like platform from developers to developers.
 * %%
 * Copyright (C) 2018 - 2019 Codenjoy
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
import {
    getBoardSize,
    getElementByXY,
    getFirstPositionOf,
    isAt,
    getAt,
    isNear,
    findPathToCoords,
    getNear,
    getNearMovable,
    coordsToCommand,
    isCoordNear
} from './utils';
import {
    ELEMENT,
    COMMANDS
} from './constants';
import { start } from 'repl';

describe("utils", () => {
    describe("getBoardSize", () => {
        it("should return board size", () => {
            const size = getBoardSize('****');
            expect(size).toEqual(2);
        });
    });

    describe("getElementByXY", () => {
        it("should returned specified element for (0,0)", () => {
            // 123|456|789
            const board = '123456789';
            const element = getElementByXY(board, { x: 0, y: 0 });
            expect(element).toEqual("1");
        });

        it("should returned specified element for (2,1)", () => {
            // 123|456|789
            const board = '123456789';
            const element = getElementByXY(board, { x: 2, y: 1 });
            expect(element).toEqual("6");
        });
    });

    describe("getFirstPositionOf", () => {
        it("should return first match", () => {
            const board = '123456789';
            const position = getFirstPositionOf(board, ['6', '9']);
            expect(position).toEqual({ x: 2, y: 1 });
        });
    });

    describe("isAt", () => {
        it("should return false for incorrect coords", () => {
            const board = '123456789';
            const res = isAt(board, 6, 3, ELEMENT.NONE);
            expect(res).toEqual(false);
        });

        it("should return 6 for (2,1)", () => {
            const board = '12345 789';
            const res = isAt(board, 2, 1, ELEMENT.NONE);
            expect(res).toEqual(true);
        });
    });

    describe("getAt", () => {
        it("should return WALL for incorrect coords", () => {
            const board = '123456789';
            const res = getAt(board, 6, 1);
            expect(res).toEqual(ELEMENT.WALL);
        });
        it("should return element for specified coords", () => {
            const board = '12345 789';
            const res = getAt(board, 2, 1);
            expect(res).toEqual(ELEMENT.NONE);
        });
    });

    describe("isNear", () => {
        it("should return WALL for incorrect coords", () => {
            const board =
                '******' +
                '* ═► *' +
                '*  ○ *' +
                '*    *' +
                '*    *' +
                '******';
            const res = isNear(board, 3, 1, '○');
            expect(res).toEqual(true);
        });
    });

    describe('getNear', () => {
        it('should return correct near coords', () => {
            const board = '12345 789';
            const near = getNear(board, { x: 1, y: 1 });
            const expectedCoords = [
                { x: 2, y: 1 },
                { x: 0, y: 1 },
                { x: 1, y: 2 },
                { x: 1, y: 0 },
            ];
            near.forEach(({ coords }, idx) => {
                expect(coords).toEqual(expectedCoords[idx]);
            });
        });

        it('should return correct near elements', () => {
            const board = '12345 789';
            const near = getNear(board, { x: 1, y: 1 });
            const expectedElements = [' ', '4', '8', '2'];
            near.forEach(({ element }, idx) => {
                expect(element).toEqual(expectedElements[idx]);
            });
        });
    });

    describe('getNearMovable', () => {
        it('should return correct coords', () => {
            const board = '1 3☼5═7●9';
            const near = getNearMovable(board, { x: 1, y: 1 });
            expect(near).toHaveLength(1);
            expect(near[0].coords).toEqual({ x: 1, y: 0 });
        });

        it('should not return walls ', () => {
            const board = '123☼5 789';
            const near = getNearMovable(board, { x: 1, y: 1 });
            const expectedElements = [' ', '8', '2'];
            near.forEach(({ element }, idx) => {
                expect(element).toEqual(expectedElements[idx]);
            });
        });
    });

    describe('findPathToCoords', () => {
        const board = '☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼' +
            '☼☼         ○                 ☼' +
            '☼#                  ×>       ☼' +
            '☼☼       ●         ○         ☼' +
            '☼☼    $      ○   ○      ○    ☼' +
            '☼☼           ●    ○          ☼' +
            '☼☼     ☼☼☼☼☼                 ☼' +
            '☼☼     ☼        $            ☼' +
            '☼#     ☼☼☼     ○  ☼☼☼☼#      ☼' +
            '☼☼     ☼          ☼   ☼  ●   ☼' +
            '☼☼     ☼☼☼☼#      ☼☼☼☼#      ☼' +
            '☼☼                ☼ ○        ☼' +
            '☼☼                ☼         $☼' +
            '☼☼    ●● ○                   ☼' +
            '☼#                 ×─>○      ☼' +
            '☼☼           ○               ☼' +
            '☼☼        ☼☼☼                ☼' +
            '☼☼   ○   ☼  ☼ ●              ☼' +
            '☼☼      ☼☼☼*ø     ☼☼   *ø    ☼' +
            '☼☼      ☼   ☼   ● ☼ ☼ ☼ ☼ ○  ☼' +
            '☼#      ☼   ☼     ☼  ☼  ☼    ☼' +
            '☼☼                ☼     ☼    ☼' +
            '☼☼     ●          ☼     ☼    ☼' +
            '☼☼                           ☼' +
            '☼☼ ╓                ○        ☼' +
            '☼☼ ║    ○      ●         ○   ☼' +
            '☼# ▼                       ● ☼' +
            '☼☼               ○           ☼' +
            '☼☼                    ○      ☼' +
            '☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼';


        it('should return correct path', () => {
            const start = Date.now();
            const path = findPathToCoords(board, { x: 8, y: 25 }, { x: 3, y: 26 });
            console.log('total time taken ', Date.now() - start) + 'ms';
        });
    });

    describe('coordsToCommand', () => {
        it('should return correct RIGHT command', () => {
            const targetCoords = { x: 2, y: 2 };
            const currentCoords = { x: 1, y: 2 };

            expect(coordsToCommand(targetCoords, currentCoords)).toBe(COMMANDS.RIGHT);
        });

        it('should return correct LEFT command', () => {
            const targetCoords = { x: 0, y: 2 };
            const currentCoords = { x: 1, y: 2 };

            expect(coordsToCommand(targetCoords, currentCoords)).toBe(COMMANDS.LEFT);
        });

        it('should return correct UP command', () => {
            const targetCoords = { x: 1, y: 1 };
            const currentCoords = { x: 1, y: 2 };

            expect(coordsToCommand(targetCoords, currentCoords)).toBe(COMMANDS.UP);
        });


        it('should return correct UP command', () => {
            const targetCoords = { x: 1, y: 3 };
            const currentCoords = { x: 1, y: 2 };

            expect(coordsToCommand(targetCoords, currentCoords)).toBe(COMMANDS.DOWN);
        });
    });

    describe('isCoordNear', () => {
        const board =
            '******' +
            '*    *' +
            '* ═► *' +
            '* ☼○☼*' +
            '*    *' +
            '******';

        it('should return true if coord is near', () => {
            expect(isCoordNear(board, { x: 3, y: 2 }, { x: 2, y: 2 })).toBeTruthy();
            expect(isCoordNear(board, { x: 3, y: 2 }, { x: 4, y: 2 })).toBeTruthy();
            expect(isCoordNear(board, { x: 3, y: 2 }, { x: 3, y: 3 })).toBeTruthy();
            expect(isCoordNear(board, { x: 3, y: 2 }, { x: 3, y: 1 })).toBeTruthy();
        });

        it('should return false if coord not near', () => {
            expect(isCoordNear(board, { x: 3, y: 2 }, { x: 5, y: 2 })).toBeFalsy();
            expect(isCoordNear(board, { x: 3, y: 2 }, { x: 1, y: 2 })).toBeFalsy();
            expect(isCoordNear(board, { x: 3, y: 2 }, { x: 3, y: 0 })).toBeFalsy();
            expect(isCoordNear(board, { x: 3, y: 2 }, { x: 3, y: 4 })).toBeFalsy();
            expect(isCoordNear(board, { x: 3, y: 2 }, { x: 4, y: 3 })).toBeFalsy();
            expect(isCoordNear(board, { x: 3, y: 2 }, { x: 2, y: 1 })).toBeFalsy();
        });
    });
});
