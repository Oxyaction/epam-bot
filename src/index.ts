/*-
 * #%L
 * Codenjoy - it's a dojo-like platform from developers to developers.
 * %%
 * Copyright (C) 2018 - 2019 Codenjoy
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
process.env['NODE_CONFIG_DIR'] = `${__dirname}/config`;
import * as config from 'config';
import { getSnakeMove2 } from './bot';
import { client as WebSocketClient } from 'websocket';
import { SnakeState } from './snake.state.class';
import { Command } from './constants';
import { Logger } from './logger/logger.class';
import { FirebaseLogTarget } from './logger/firebase-log-target.class';
import { LoggerTargetInterface } from './logger/logger-target.interface';
import { Firebase } from './firebase.class';

let url = config.get('gameServerConnection.domain') + '/codenjoy-contest/board/player/{EMAIL}?code={TOKEN}';
url = url
    .replace("http", "ws")
    .replace("board/player/", "ws?user=")
    .replace("?code=", "&code=")
    .replace('{EMAIL}', config.get('gameServerConnection.email'))
    .replace('{TOKEN}', config.get('gameServerConnection.token'));

console.log(url);

const client = new WebSocketClient();

client.on('connectFailed', function (error) {
    console.error('Connect Error: ' + error.toString());
});

client.on('connect', function (connection) {
    connection.on('error', function (error) {
        console.error("Connection Error: " + error.toString());
    });

    connection.on('close', function () {
        console.log('echo-protocol Connection Closed');
    });

    connection.on('message', onMessage);

    async function onMessage(event) {
        if (connection.connected) {
            var pattern = new RegExp(/^board=(.*)$/);
            var message = event.utf8Data;
            var parameters = message.match(pattern);
            var board = parameters[1];
            var answer = await processBoard(board);
            connection.sendUTF(answer);
        }
    }
});

client.connect(url);

const firebase = new Firebase();
firebase.init();
const logTarget: LoggerTargetInterface = new FirebaseLogTarget(firebase, config.get('logsCollectionName'));
const logger: Logger = new Logger(logTarget);
const state: SnakeState = new SnakeState();
let currentBoard;

async function processBoard(board): Promise<string> {
    currentBoard = board;
    try {
        return getSnakeMove2(board, state, logger);
    } catch (err) {
        console.error(err);
        logger.log({
            board,
        })
        await logger.flush()
        throw err;
    }
}

process.on('SIGINT', () => {
    if (currentBoard) {
        logger.log({
            board: currentBoard,
        });
    }
    logger.flush();
    firebase.close();
    process.exit(0);
});
