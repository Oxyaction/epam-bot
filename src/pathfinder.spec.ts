import { Pathfinder } from './pathfinder.class';
import { EatingStrategy } from './strategies/eating-strategy.class';
import { AttackingStrategy } from './strategies/attacking-strategy.class';
import { InterceptionStrategy } from './strategies/interception-strategy.class';
import { Board, Coordinates } from './board.class';
import { SnakeState } from './snake.state.class';
import { Snake } from './snake.class';
import { AbstractStrategy } from './strategies/abstract-strategy.class';

describe('pathfinder', () => {
  const init = (boardAsString: string, snakeState?: SnakeState) => {
    const board = new Board(boardAsString);
    const snake: Snake = board.getPlayer();
    if (snakeState) {
      snake.setState(snakeState)
    } else {
      snake.setState(new SnakeState());
    }
    const finder = new Pathfinder();
    const strategy = new EatingStrategy(board, finder);
    return {
      finder,
      board,
      strategy,
      snake,
    }
  }

  describe('findPathToCoords', () => {
    it('should find correct path', () => {
      const boardAsString =
        '☼☼☼☼☼☼' +
        '☼▲   ☼' +
        '☼╙   ☼' +
        '☼   ○☼' +
        '☼    ☼' +
        '☼☼☼☼☼☼';
      const { board, finder } = init(boardAsString);
      const path: Coordinates[] = finder.findPathToCoords(board, { x: 4, y: 3 }, board.getPlayer().head.coordinates, board.getPlayer());
      expect(path).toEqual([{ "x": 2, "y": 1 }, { "x": 2, "y": 2 }, { "x": 2, "y": 3 }, { "x": 3, "y": 3 }, { "x": 4, "y": 3 }]);
    });

    it('should not return path if target unreachable', () => {
      const boardAsString =
        '☼☼☼☼☼☼' +
        '☼▲   ☼' +
        '☼╙ ☼ ☼' +
        '☼ ☼○☼☼' +
        '☼  ☼ ☼' +
        '☼☼☼☼☼☼';
      const { board, finder } = init(boardAsString);
      // const board = new Board(boardAsString);
      // const finder = new Pathfinder();
      const path: Coordinates[] = finder.findPathToCoords(board, { x: 3, y: 3 }, board.getPlayer().head.coordinates, board.getPlayer());
      expect(path).toHaveLength(0);
      expect(path).toEqual([]);
    });

    it('should return path if fury works', () => {
      const boardAsString =
        '☼☼☼☼☼☼' +
        '☼♥   ☼' +
        '☼╙ ● ☼' +
        '☼ ☼○☼☼' +
        '☼  ☼ ☼' +
        '☼☼☼☼☼☼';
      const state = new SnakeState();
      state.furyDuration = 3;
      const { board, finder } = init(boardAsString, state);
      // const board = new Board(boardAsString);
      // const finder = new Pathfinder();
      const path: Coordinates[] = finder.findPathToCoords(board, { x: 3, y: 2 }, board.getPlayer().head.coordinates, board.getPlayer());
      expect(path).toHaveLength(3);
      expect(path).toEqual([{ "x": 2, "y": 1 }, { "x": 2, "y": 2 }, { "x": 3, "y": 2 }]);
    });

    // currently avoiding rocks when not in fury
    it.skip('should return path if snake survives collission', () => {
      const boardAsString =
        '☼☼☼☼☼☼' +
        '☼▲   ☼' +
        '☼║ ● ☼' +
        '☼║☼○☼☼' +
        '☼╚╕☼ ☼' +
        '☼☼☼☼☼☼';
      const { board, finder } = init(boardAsString);
      // const board = new Board(boardAsString);
      // const finder = new Pathfinder();
      const path: Coordinates[] = finder.findPathToCoords(board, { x: 3, y: 3 }, board.getPlayer().head.coordinates, board.getPlayer());
      expect(path).toHaveLength(4);
      expect(path).toEqual([{ "x": 2, "y": 1 }, { "x": 3, "y": 1 }, { "x": 3, "y": 2 }, { "x": 3, "y": 3 }]);
    });

    it('should not return path if snake doesnt survive collission', () => {
      const boardAsString =
        '☼☼☼☼☼☼' +
        '☼▲   ☼' +
        '☼╙ ● ☼' +
        '☼ ☼○☼☼' +
        '☼  ☼ ☼' +
        '☼☼☼☼☼☼';
      const { board, finder } = init(boardAsString);
      // const board = new Board(boardAsString);
      // const finder = new Pathfinder();
      const path: Coordinates[] = finder.findPathToCoords(board, { x: 3, y: 3 }, board.getPlayer().head.coordinates, board.getPlayer());
      expect(path).toHaveLength(0);
      expect(path).toEqual([]);
    });

  });

  describe('isSafePath', () => {

    const isSafePath = (boardAsString: string, to: Coordinates, state?: SnakeState): boolean => {
      const { board, finder } = init(boardAsString, state);
      const path: Coordinates[] = finder.findPathToCoords(board, to, board.getPlayer().head.coordinates, board.getPlayer());
      const strategies: AbstractStrategy[] = [
        new AttackingStrategy(board, finder),
        new InterceptionStrategy(board, finder),
        new EatingStrategy(board, finder)
      ];
      return finder.isSafePath(board, path, board.getPlayer(), strategies);
    };

    it('should consider path as safe by default', () => {
      const boardAsString =
        '☼☼☼☼☼☼☼☼' +
        '☼▲     ☼' +
        '☼╙     ☼' +
        '☼      ☼' +
        '☼      ☼' +
        '☼    ○ ☼' +
        '☼      ☼' +
        '☼☼☼☼☼☼☼☼';
      expect(isSafePath(boardAsString, { x: 5, y: 5 })).toBeTruthy();
    });

    it('should not consider dead end path as safe', () => {
      const boardAsString =
        '☼☼☼☼☼☼☼☼' +
        '☼▲     ☼' +
        '☼╙     ☼' +
        '☼   ☼○☼☼' +
        '☼   ☼ ☼☼' +
        '☼   ☼☼☼☼' +
        '☼      ☼' +
        '☼☼☼☼☼☼☼☼';
      expect(isSafePath(boardAsString, { x: 5, y: 3 })).toBeFalsy();
    });

    it('should not return collide path with same enemy as safe', () => {
      const boardAsString =
        '☼☼☼☼☼☼☼☼' +
        '☼      ☼' +
        '☼      ☼' +
        '☼╘► ○  ☼' +
        '☼      ☼' +
        '☼   ˄  ☼' +
        '☼   ¤  ☼' +
        '☼☼☼☼☼☼☼☼';

      expect(isSafePath(boardAsString, { x: 4, y: 3 })).toBeFalsy();
    });

    it('should return collide path with same enemy as safe when snake in fury', () => {
      const boardAsString =
        '☼☼☼☼☼☼☼☼' +
        '☼      ☼' +
        '☼      ☼' +
        '☼╘♥ ○  ☼' +
        '☼      ☼' +
        '☼   ˄  ☼' +
        '☼   ¤  ☼' +
        '☼☼☼☼☼☼☼☼';

      const snakeState: SnakeState = new SnakeState();
      snakeState.furyDuration = 2;
      expect(isSafePath(boardAsString, { x: 4, y: 3 }, snakeState)).toBeTruthy();
    });

    it('should not return collide path with enemy as safe when snake in fury but fury finishes before collide', () => {
      const boardAsString =
        '☼☼☼☼☼☼☼☼' +
        '☼      ☼' +
        '☼      ☼' +
        '☼╘♥ ○  ☼' +
        '☼      ☼' +
        '☼   ˄  ☼' +
        '☼   ¤  ☼' +
        '☼☼☼☼☼☼☼☼';
      const snakeState: SnakeState = new SnakeState();
      snakeState.furyDuration = 1;
      expect(isSafePath(boardAsString, { x: 4, y: 3 }, snakeState)).toBeFalsy();
    });

    it('should return true if snake will survive collision', () => {
      const boardAsString =
        '☼☼☼☼☼☼☼☼' +
        '☼╔╕    ☼' +
        '☼║     ☼' +
        '☼╚► ○  ☼' +
        '☼      ☼' +
        '☼   ˄  ☼' +
        '☼   ¤  ☼' +
        '☼☼☼☼☼☼☼☼';
      expect(isSafePath(boardAsString, { x: 4, y: 3 })).toBeTruthy();
    });
  });
});
