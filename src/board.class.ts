import {
  Command,
  EnemyTail,
  EnemyHead,
  Element,
  PlayerTail,
  PlayerHead,
} from './constants';
import { Snake } from './snake.class';

export type Coordinates = {
  x: number,
  y: number,
  distance?: number,
}

export type BoardObject = {
  element: Element,
  coordinates: Coordinates,
  value?: number
};

export type MovableFilter = (boardObject: BoardObject, fromCoordinates: Coordinates) => boolean;

// export enum Enemy

export class Board {
  public readonly size;
  private player: Snake;
  private enemies: Snake[] = [];
  private board: Element[];

  static coordinatesToString({ x, y }: Coordinates): string {
    return `${x}-${y}`;
  }

  static coordinatesEqual(coord1: Coordinates, coord2: Coordinates): boolean {
    return coord1.x === coord2.x && coord1.y === coord2.y;
  }

  static countDistance(coordinates1: Coordinates, coordinates2: Coordinates): number {
    return Math.sqrt(Math.pow(coordinates1.x - coordinates2.x, 2)
      + Math.pow(coordinates1.y - coordinates2.y, 2));
  }

  static commandToCoordinates(position: Coordinates, command: Command): Coordinates {
    const commandsMapping = {
      [Command.LEFT]: { x: position.x - 1, y: position.y },
      [Command.RIGHT]: { x: position.x + 1, y: position.y },
      [Command.UP]: { x: position.x, y: position.y - 1 },
      [Command.DOWN]: { x: position.x, y: position.y + 1 },
    };
    return commandsMapping[command];
  }

  static coordinatesToCommand(targetCoords: Coordinates, currentCoords: Coordinates): Command {
    const adjacementCoordsMapping = {
      [this.coordinatesToString({ x: currentCoords.x + 1, y: currentCoords.y })]: Command.RIGHT,
      [this.coordinatesToString({ x: currentCoords.x - 1, y: currentCoords.y })]: Command.LEFT,
      [this.coordinatesToString({ x: currentCoords.x, y: currentCoords.y + 1 })]: Command.DOWN,
      [this.coordinatesToString({ x: currentCoords.x, y: currentCoords.y - 1 })]: Command.UP,
    }
    return adjacementCoordsMapping[this.coordinatesToString(targetCoords)];
  }

  static getMinDistanceBetweenCoordinates(from: Coordinates, to: Coordinates): number {
    return Math.min(Math.abs(from.x - to.x), Math.abs(from.y - to.y));
  }

  static getNearCoordinatesIn({ x, y }: Coordinates, distance: number, returnDistance: boolean = false): Coordinates[] {
    const coorinates: Coordinates[] = [];
    for (let i = -distance; i <= distance; i++) {
      for (let j = Math.abs(i) - distance; j <= distance - Math.abs(i); j++) {
        // do not return fromCoordinates
        if (j === 0 && i === 0) {
          continue;
        }
        const targetCoordinates: Coordinates = { x: x + i, y: y + j };
        if (returnDistance) {
          targetCoordinates.distance = Math.abs(i) + Math.abs(j);
        }
        coorinates.push(targetCoordinates);
      }
    }
    return coorinates;
  }

  constructor(board: string, scan: boolean = true) {
    this.board = board as any as Element[];
    this.size = Math.sqrt(this.board.length);
    if (scan) {
      this.scan();
    }
  }

  public scan() {
    this.scanPlayer();
    this.scanEnemies();
  }

  public getEnemies(): Snake[] {
    return this.enemies.filter(enemy => enemy.isActive);
  }

  public getPlayer(): Snake {
    return this.player;
  }

  public getCoordinatesByPosition(position: number): Coordinates {
    if (position === -1) {
      return null;
    }

    return {
      x: position % this.size,
      y: (position - (position % this.size)) / this.size
    };
  }

  public getElementByCoordinates(coordinates: Coordinates): Element {
    return this.board[this.size * coordinates.y + coordinates.x];
  }

  public scanPlayer() {
    // cannot scan if sleeping
    this.player = this.restoreSnakeByTailPosition(this.getUserTail()) || new Snake([]);
  }

  public scanEnemies() {
    const enemyTailsCoordinates: Coordinates[] = this.getEnemyTails();
    this.enemies = enemyTailsCoordinates
      .map(tailCoordinates => this.restoreSnakeByTailPosition(tailCoordinates))
      .filter(value => !!value);
  }

  public getNear({ x, y }: Coordinates): BoardObject[] {
    return Board.getNearCoordinatesIn({ x, y }, 1).map((coordinates) => ({
      coordinates,
      element: this.getElementByCoordinates(coordinates)
    }));
  }

  // public getNearMovable(coordinates: Coordinates, snake?: Snake, distance?: number) {
  public getNearMovable(coordinates: Coordinates, movableFilter: MovableFilter) {
    const nearCoords = this.getNear(coordinates);
    // return nearCoords.filter(({ element }) => this.isMovableElement(element, snake, distance));
    return nearCoords.filter((boardObject: BoardObject) => movableFilter(boardObject, coordinates));
  }

  public setElement(coordinates: Coordinates, element: Element): void {
    const size = this.size;
    if (this.isOut(coordinates)) {
      return;
    }
    let boardAsString = this.board as any as string;
    const idx = size * coordinates.y + coordinates.x;
    boardAsString = boardAsString.substr(0, idx) + element + boardAsString.substr(idx + 1);
    this.board = boardAsString as any as Element[];
  }

  public copy(): Board {
    const boardAsString = this.board as any as string;
    return new Board(boardAsString, false);
  }

  public getEnemySnakeByCoordinate(coordinates: Coordinates): Snake {
    return this.getEnemies().find(snake => snake.isCoordinateBelongsToSnake(coordinates));
  }

  public getBoardAsDrawableString(): string {
    const result = [];
    const boardAsString = this.board as any as string;
    for (var i = 0; i < this.size; i++) {
      result.push(boardAsString.substring(i * this.size, (i + 1) * this.size));
    }
    return result.join("\n");
  }

  public getEnemiesInRangeOfPlayer(range: number = 5): Snake[] {
    return this.getEnemiesInRange(this.getPlayer().head.coordinates, range);
  }

  public getEnemiesInRange(coordinates: Coordinates, range: number = 5): Snake[] {
    const enemiesInRange: Snake[] = [];
    for (let i = 0; i < this.getEnemies().length; i++) {
      const enemy: Snake = this.getEnemies()[i];
      if (Board.getMinDistanceBetweenCoordinates(coordinates, enemy.head.coordinates) <= range) {
        enemiesInRange.push(enemy);
      }
    }
    return enemiesInRange;
  }

  public isOut({ x, y }: Coordinates) {
    return x >= this.size || y >= this.size || x < 0 || y < 0;
  }

  public getStrongestEnemy(): Snake {
    return this.getEnemies().sort((enemyA, enemyB) => enemyA.length > enemyB.length ? -1 : 1)[0];
  }

  public isPlayerSleeping(): boolean {
    return this.isElementExists(Element.HEAD_SLEEP);
  }

  public isPlayerDead(): boolean {
    return this.isElementExists(Element.HEAD_DEAD);
  }

  private getUserTail(): Coordinates {
    const tails = Object.values(PlayerTail);
    for (let i = 0; i < this.board.length; i++) {
      if (tails.includes(this.board[i])) {
        return this.getCoordinatesByPosition(i);
      }
    }
  }

  private isElementExists(element: Element): boolean {
    for (let i = 0; i < this.board.length; i++) {
      if (this.board[i] === element) {
        return true;
      }
    }
    return false;
  }

  private getEnemyTails(): Coordinates[] {
    const enemyTails: Coordinates[] = [];
    const tails = Object.values(EnemyTail);

    for (let i = 0; i < this.board.length; i++) {
      if (tails.includes(this.board[i])) {
        enemyTails.push(this.getCoordinatesByPosition(i));
      }
    }
    return enemyTails;
  }

  private restoreSnakeByTailPosition(tailPosition: Coordinates): Snake {
    if (!tailPosition) {
      return null;
    }
    let currentElement: Element = this.getElementByCoordinates(tailPosition);
    const isPlayer = Object.values(PlayerTail).includes(currentElement);
    let elementToCommand;
    if (isPlayer) {
      elementToCommand = {
        [Element.BODY_HORIZONTAL]: from => from === Command.RIGHT ? Command.RIGHT : Command.LEFT,
        [Element.BODY_VERTICAL]: from => from === Command.DOWN ? Command.DOWN : Command.UP,

        [Element.BODY_LEFT_DOWN]: from => from === Command.RIGHT ? Command.DOWN : Command.LEFT,
        [Element.BODY_LEFT_UP]: from => from === Command.RIGHT ? Command.UP : Command.LEFT,

        [Element.BODY_RIGHT_DOWN]: from => from === Command.LEFT ? Command.DOWN : Command.RIGHT,
        [Element.BODY_RIGHT_UP]: from => from === Command.LEFT ? Command.UP : Command.RIGHT,

        [Element.TAIL_END_DOWN]: () => Command.UP,
        [Element.TAIL_END_UP]: () => Command.DOWN,
        [Element.TAIL_END_RIGHT]: () => Command.LEFT,
        [Element.TAIL_END_LEFT]: () => Command.RIGHT,
      };
    } else {
      elementToCommand = {
        [Element.ENEMY_BODY_HORIZONTAL]: from => from === Command.RIGHT ? Command.RIGHT : Command.LEFT,
        [Element.ENEMY_BODY_VERTICAL]: from => from === Command.DOWN ? Command.DOWN : Command.UP,

        [Element.ENEMY_BODY_LEFT_DOWN]: from => from === Command.RIGHT ? Command.DOWN : Command.LEFT,
        [Element.ENEMY_BODY_LEFT_UP]: from => from === Command.RIGHT ? Command.UP : Command.LEFT,

        [Element.ENEMY_BODY_RIGHT_DOWN]: from => from === Command.LEFT ? Command.DOWN : Command.RIGHT,
        [Element.ENEMY_BODY_RIGHT_UP]: from => from === Command.LEFT ? Command.UP : Command.RIGHT,

        [Element.ENEMY_TAIL_END_DOWN]: () => Command.UP,
        [Element.ENEMY_TAIL_END_UP]: () => Command.DOWN,
        [Element.ENEMY_TAIL_END_RIGHT]: () => Command.LEFT,
        [Element.ENEMY_TAIL_END_LEFT]: () => Command.RIGHT,
      };
    }

    const enemyHeads = Object.values(EnemyHead);
    const playerHeads = Object.values(PlayerHead)

    let snakeElements: BoardObject[] = [];

    let prevCommand: Command = null;
    let currentCoords: Coordinates = tailPosition;
    snakeElements.push({
      coordinates: currentCoords,
      element: currentElement
    });

    while (!enemyHeads.includes(currentElement) && !(playerHeads.includes(currentElement))) {
      if (!elementToCommand[currentElement]) {
        return null;
      }
      const command = elementToCommand[currentElement](prevCommand);
      const newCords = Board.commandToCoordinates(currentCoords, command);
      currentElement = this.getElementByCoordinates(newCords);
      // eating case
      if ((isPlayer && enemyHeads.includes(currentElement)) || (!isPlayer && playerHeads.includes(currentElement))) {
        return null;
      }
      currentCoords = newCords;
      snakeElements.push({
        coordinates: currentCoords,
        element: currentElement,
      });
      prevCommand = command;
    }

    return new Snake(snakeElements.reverse());
  }

  public getBoardAsString(): string {
    return this.board as any as string;
  }
}
