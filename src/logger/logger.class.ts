import { Coordinates } from "../board.class";
import { LoggerTargetInterface } from "./logger-target.interface";

export interface LogEntry {
  board: string,
  tick?: number,
  // possibleTargets: Coordinates[],
  predictableEnemyPaths?: Coordinates[][],
  path?: Coordinates[],
  debug?: {
    [key: string]: any
  },
}

const padZero = (num: number): string => num < 10 ? `0${num}` : `${num}`;

export class Logger {
  private _logs: LogEntry[] = [];
  private _sessionName: string;
  private enemyPaths: Coordinates[][] = [];
  private _debug: { [key: string]: any } = {};
  private _isDead: boolean = false;

  constructor(private readonly logTarget: LoggerTargetInterface) { }

  public init(): void {
    this._sessionName = this.generateSessionName();
    this.reset();
  }

  public reset(): void {
    this._logs = [];
    this.enemyPaths = [];
    this._debug = {};
    this._isDead = false;
  }

  public log(logEntry: LogEntry): void {
    logEntry.predictableEnemyPaths = this.enemyPaths;
    logEntry.debug = { ...logEntry.debug, ...this._debug };
    this._logs.push(logEntry);
    this.enemyPaths = [];
  }

  public debug(key: string, value: any): void {
    this._debug[key] = value;
  }

  public logEnemyPath(path: Coordinates[]): void {
    this.enemyPaths.push(path);
  }

  public setDead() {
    this._isDead = true;
  }

  public flush(): void {
    if (this._logs.length > 30) {
      // flush only > 30 ticks games
      this.logTarget.flush(this._sessionName, this._logs, this._isDead);
      console.log(`log flushed ${this._sessionName}, log size is ${this._logs.length} ticks, ${this._isDead ? 'lose' : 'win'}`);
    }
    this.reset();
  }

  private generateSessionName(): string {
    const currentdate = new Date();
    return `${currentdate.getFullYear()}-${padZero(currentdate.getMonth() + 1)}-${padZero(currentdate.getDate())}` +
      ` ${padZero(currentdate.getHours())}:${padZero(currentdate.getMinutes())}:${padZero(currentdate.getSeconds())}`;
  }
}
