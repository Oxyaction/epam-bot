import { Firebase } from "../firebase.class";
import { LogEntry } from "./logger.class";
import { LoggerTargetInterface } from './logger-target.interface';

export class FirebaseLogTarget implements LoggerTargetInterface {
  constructor(private readonly firebase: Firebase, private readonly collectionName: string) { }

  public flush(sessionName: string, logs: LogEntry[], isDead: boolean): Promise<void> {
    return this.firebase.write(this.collectionName, sessionName, {
      logs: JSON.stringify(logs),
      date: new Date(),
      dead: isDead,
      ticksCount: logs.length
    });
  }
}
