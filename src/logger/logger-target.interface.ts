import { LogEntry } from "./logger.class";

export interface LoggerTargetInterface {
  flush(sessionName: string, logs: LogEntry[], isDead: boolean): Promise<void>;
}
