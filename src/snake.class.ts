import { BoardObject, Coordinates, Board } from "./board.class";
import {
  Element,
  Command,
  PlayerHead,
  EnemyTail,
} from './constants';
import {
  SnakeState
} from './snake.state.class';

export class Snake {

  private state: SnakeState;

  constructor(private coords: BoardObject[]) { }

  public setState(state: SnakeState): void {
    this.state = state;
  }

  public getState(): SnakeState {
    return this.state;
  }

  get coordinates(): Coordinates[] {
    return this.coords.map(boardObject => boardObject.coordinates);
  }

  get length(): number {
    return this.coords.length;
  }

  get isFury(): boolean {
    return this.isEnemy ? this.head.element === Element.ENEMY_HEAD_EVIL : this.state.furyDuration > 0;
  }

  get isFlying(): boolean {
    return this.isEnemy ? this.head.element === Element.ENEMY_HEAD_FLY : this.state.flyingDuration > 0;
  }

  get head(): BoardObject {
    return this.coords[0];
  }

  get neck(): BoardObject {
    return this.coords[1];
  }

  get tail(): BoardObject {
    return this.coords[this.length - 1];
  }

  get isPlayer(): boolean {
    return Object.values(PlayerHead).includes(this.head.element) && !Object.values(EnemyTail).includes(this.tail.element);
  }

  get isEnemy(): boolean {
    return !this.isPlayer;
  }

  get isSleeping(): boolean {
    if (!this.coords.length) {
      return true;
    }
    const sleepingHeads = [
      Element.HEAD_SLEEP,
      Element.ENEMY_HEAD_SLEEP,
    ];
    return sleepingHeads.includes(this.head.element);
  }

  get isDead(): boolean {
    const headHeads = [
      Element.HEAD_DEAD,
      Element.ENEMY_HEAD_DEAD,
    ];
    return headHeads.includes(this.head.element);
  }

  get isActive(): boolean {
    return !this.isSleeping && !this.isDead;
  }

  get direction(): Command {
    const headToDirection = {
      [Element.HEAD_UP]: Command.UP,
      [Element.ENEMY_HEAD_UP]: Command.UP,
      [Element.HEAD_DOWN]: Command.DOWN,
      [Element.ENEMY_HEAD_DOWN]: Command.DOWN,
      [Element.HEAD_LEFT]: Command.LEFT,
      [Element.ENEMY_HEAD_LEFT]: Command.LEFT,
      [Element.HEAD_RIGHT]: Command.RIGHT,
      [Element.ENEMY_HEAD_RIGHT]: Command.RIGHT
    };
    return headToDirection[this.head.element];
  }

  get possibleDirections(): Command[] {
    const { direction } = this;
    if (!direction) {
      return [];
    }
    const possibleDirections = {
      [Command.UP]: [Command.LEFT, Command.RIGHT, Command.UP],
      [Command.DOWN]: [Command.LEFT, Command.RIGHT, Command.DOWN],
      [Command.LEFT]: [Command.LEFT, Command.DOWN, Command.UP],
      [Command.RIGHT]: [Command.DOWN, Command.RIGHT, Command.UP],
    }
    return possibleDirections[direction];
  }

  public isCoordinateBelongsToSnake(coords: Coordinates): boolean {
    return this.coordinates.some(_coords => Board.coordinatesEqual(_coords, coords));
  }

  public isNeckOrHead(coords: Coordinates): boolean {
    return Board.coordinatesEqual(coords, this.head.coordinates) || Board.coordinatesEqual(coords, this.neck.coordinates);
  }

  public moveToPath(board: Board, path: Coordinates[]): void {
    // erase old coordinates from board
    this.coordinates.forEach((coorinates) => board.setElement(coorinates, Element.NONE));

    path.forEach((coordinates) => {
      this.coords.pop();
      // process head
      let command: Command = Board.coordinatesToCommand(coordinates, this.head.coordinates);
      switch (command) {
        case Command.DOWN:
          this.coords.unshift({ coordinates, element: this.isPlayer ? Element.HEAD_DOWN : Element.ENEMY_HEAD_DOWN });
          break;
        case Command.UP:
          this.coords.unshift({ coordinates, element: this.isPlayer ? Element.HEAD_UP : Element.ENEMY_HEAD_UP });
          break;
        case Command.LEFT:
          this.coords.unshift({ coordinates, element: this.isPlayer ? Element.HEAD_LEFT : Element.ENEMY_HEAD_LEFT });
          break;
        case Command.RIGHT:
          this.coords.unshift({ coordinates, element: this.isPlayer ? Element.HEAD_RIGHT : Element.ENEMY_HEAD_RIGHT });
          break;
        default:
          throw new Error('Neck is not adjacement to head');
      }
      // process body
      for (let i = 1; i < this.coords.length - 1; i++) {
        const segment: Coordinates = this.coordinates[i];
        const prevSegment: Coordinates = this.coordinates[i - 1];
        const nextSegment: Coordinates = this.coordinates[i + 1];
        const toPrevSegment: Command = Board.coordinatesToCommand(prevSegment, segment);
        const toNextSegment: Command = Board.coordinatesToCommand(nextSegment, segment);

        if (
          toPrevSegment === Command.UP && toNextSegment === Command.DOWN
          || toPrevSegment === Command.DOWN && toNextSegment === Command.UP
        ) {
          this.coords[i].element = this.isPlayer ? Element.BODY_VERTICAL : Element.ENEMY_BODY_VERTICAL
        } else if (
          toPrevSegment === Command.LEFT && toNextSegment === Command.RIGHT
          || toPrevSegment === Command.RIGHT && toNextSegment === Command.LEFT
        ) {
          this.coords[i].element = this.isPlayer ? Element.BODY_HORIZONTAL : Element.ENEMY_BODY_HORIZONTAL
        } else if (
          toPrevSegment === Command.LEFT && toNextSegment === Command.DOWN
          || toPrevSegment === Command.DOWN && toNextSegment === Command.LEFT
        ) {
          this.coords[i].element = this.isPlayer ? Element.BODY_LEFT_DOWN : Element.ENEMY_BODY_LEFT_DOWN;
        } else if (
          toPrevSegment === Command.LEFT && toNextSegment === Command.UP
          || toPrevSegment === Command.UP && toNextSegment === Command.LEFT
        ) {
          this.coords[i].element = this.isPlayer ? Element.BODY_LEFT_UP : Element.ENEMY_BODY_LEFT_UP;
        } else if (
          toPrevSegment === Command.UP && toNextSegment === Command.RIGHT
          || toPrevSegment === Command.RIGHT && toNextSegment === Command.UP
        ) {
          this.coords[i].element = this.isPlayer ? Element.BODY_RIGHT_UP : Element.ENEMY_BODY_RIGHT_UP;
        } else if (
          toPrevSegment === Command.DOWN && toNextSegment === Command.RIGHT
          || toPrevSegment === Command.RIGHT && toNextSegment === Command.DOWN
        ) {
          this.coords[i].element = this.isPlayer ? Element.BODY_RIGHT_DOWN : Element.ENEMY_BODY_RIGHT_DOWN;
        }
      }
      // process tail
      command = Board.coordinatesToCommand(this.tail.coordinates, this.coordinates[this.coordinates.length - 2]);
      switch (command) {
        case Command.LEFT:
          this.tail.element = this.isPlayer ? Element.TAIL_END_LEFT : Element.ENEMY_TAIL_END_LEFT;
          break;
        case Command.RIGHT:
          this.tail.element = this.isPlayer ? Element.TAIL_END_RIGHT : Element.ENEMY_TAIL_END_RIGHT;
          break;
        case Command.UP:
          this.tail.element = this.isPlayer ? Element.TAIL_END_UP : Element.ENEMY_TAIL_END_UP;
          break;
        case Command.DOWN:
          this.tail.element = this.isPlayer ? Element.TAIL_END_DOWN : Element.ENEMY_TAIL_END_DOWN;
          break;
      }
    });

    this.coords.forEach(({ coordinates, element }) => board.setElement(coordinates, element));
  }

  public isStrongerThen(otherSnake: Snake, distance: number) {
    if (otherSnake.isFury) {
      if (this.isFury && this.state && this.state.furyDuration >= distance
        && this.length - otherSnake.length > 1) {
        return true;
      }
      return false;
    }
    if (this.isFury && (this.state && this.state.furyDuration >= distance)) {
      return true;
    }
    return this.length - otherSnake.length > 1;
  }

  public canEatStone(distance: number): boolean {
    return (
      this.length - 3 > 1
      || (this.isFury && ((this.getState() && this.getState().furyDuration >= distance) || !this.getState()))
    );
  }

  public copy() {
    const snake = new Snake(this.coords.map(coord => ({ ...coord })));
    if (this.state) {
      snake.setState(this.state.copy());
    }
    return snake;
  }
}
