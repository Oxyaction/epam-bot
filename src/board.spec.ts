import {
  Board,
  BoardObject,
} from './board.class';
import {
  Command,
  Element,
  ELEMENT
} from './constants';
import { Snake } from './snake.class';

describe('Board', () => {

  describe('getCoordinateByPosition', () => {
    it('should return correct coordinate', () => {
      const boardAsString =
        '☼☼☼☼☼☼' +
        '☼    ☼' +
        '☼  ● ☼' +
        '☼    ☼' +
        '☼    ☼' +
        '☼☼☼☼☼☼';

      const board = new Board(boardAsString, false);
      expect(board.getCoordinatesByPosition(15)).toEqual({ x: 3, y: 2 });
    })
  });

  describe('commandToCoordinates', () => {
    it('should return correct coordinate', () => {
      expect(Board.commandToCoordinates({ x: 5, y: 6 }, Command.DOWN)).toEqual({ x: 5, y: 7 });
      expect(Board.commandToCoordinates({ x: 5, y: 6 }, Command.UP)).toEqual({ x: 5, y: 5 });
      expect(Board.commandToCoordinates({ x: 5, y: 6 }, Command.LEFT)).toEqual({ x: 4, y: 6 });
      expect(Board.commandToCoordinates({ x: 5, y: 6 }, Command.RIGHT)).toEqual({ x: 6, y: 6 });
    });
  });

  describe('getEnemies', () => {
    it('should return active enemies snakes', () => {
      const boardAsString =
        '☼☼☼☼☼☼' +
        '☼˄æ<┐☼' +
        '☼│☺ ¤☼' +
        '☼└ö æ☼' +
        '☼×─>♣☼' +
        '☼☼☼☼☼☼';

      const board = new Board(boardAsString, false);
      board.scanEnemies();
      const enemies: Snake[] = board.getEnemies();
      expect(enemies).toHaveLength(4);
    });
  });

  describe('scanPlayer', () => {
    it('should return player snake', () => {
      const boardAsString =
        '☼☼☼☼☼☼' +
        '☼╘╗  ☼' +
        '☼ ║  ☼' +
        '☼ ▼  ☼' +
        '☼    ☼' +
        '☼☼☼☼☼☼';

      const board = new Board(boardAsString);
      board.scanPlayer();
      const player: Snake = board.getPlayer();
      expect(player.length).toBe(4);
    });
  });

  describe('getNear', () => {
    it('should return correct near coords', () => {
      const boardAsString =
        '☼☼☼' +
        '☼ ☼' +
        '☼☼☼';

      const board = new Board(boardAsString, false);
      const near: BoardObject[] = board.getNear({ x: 1, y: 1 });

      const expectedCoords = [
        { x: 0, y: 1 },
        { x: 1, y: 0 },
        { x: 1, y: 2 },
        { x: 2, y: 1 },
      ];
      near.forEach(({ coordinates }, idx) => {
        expect(coordinates).toEqual(expectedCoords[idx]);
      });
    });

    it('should return correct near elements', () => {
      const boardAsString =
        '123' +
        '45☼' +
        '789';

      const board = new Board(boardAsString, false);
      const near: BoardObject[] = board.getNear({ x: 1, y: 1 });

      const expectedElements = ['4', '2', '8', '☼'];
      near.forEach(({ element }, idx) => {
        expect(element).toEqual(expectedElements[idx]);
      });
    });
  });

  describe('getNearCoordinatesIn', () => {
    it('should return coords in 1 move near', () => {
      const coordinates = Board.getNearCoordinatesIn({ x: 3, y: 3 }, 1);
      expect(coordinates).toHaveLength(4);
      expect(coordinates).toEqual([{ x: 2, y: 3 }, { x: 3, y: 2 }, { x: 3, y: 4 }, { x: 4, y: 3 }]);
    });

    it('should return coords in 2 move near', () => {
      const coordinates = Board.getNearCoordinatesIn({ x: 3, y: 3 }, 2);
      expect(coordinates).toEqual([
        { x: 1, y: 3 },
        { x: 2, y: 2 },
        { x: 2, y: 3 },
        { x: 2, y: 4 },
        { x: 3, y: 1 },
        { x: 3, y: 2 },
        { x: 3, y: 4 },
        { x: 3, y: 5 },
        { x: 4, y: 2 },
        { x: 4, y: 3 },
        { x: 4, y: 4 },
        { x: 5, y: 3 }
      ]);
      expect(coordinates).toHaveLength(12);
    });
  });

  describe.skip('getNearMovable', () => {
    // it('should return correct coords', () => {
    //   const boardAsString = '1 3☼5═7●9';
    //   const board = new Board(boardAsString, false);
    //   const near = board.getNearMovable({ x: 1, y: 1 }, Board.isMovableElement);
    //   expect(near).toHaveLength(3);
    //   expect(near).toEqual([
    //     { "coordinates": { "x": 2, "y": 1 }, "element": "═" },
    //     { "coordinates": { "x": 1, "y": 2 }, "element": "●" },
    //     { "coordinates": { "x": 1, "y": 0 }, "element": " " }
    //   ]);
    // });

    // it('should not return walls ', () => {
    //   const boardAsString = '123☼5 789';
    //   const board = new Board(boardAsString, false);
    //   const near = board.getNearMovable({ x: 1, y: 1 }, Board.isMovableElement);
    //   const expectedElements = [' ', '8', '2'];
    //   near.forEach(({ element }, idx) => {
    //     expect(element).toEqual(expectedElements[idx]);
    //   });
    // });

    // it('should return stones for snake with size 5', () => {
    //   const boardAsString =
    //     '☼☼☼☼☼☼' +
    //     '☼╔═► ☼' +
    //     '☼║ ● ☼' +
    //     '☼╙   ☼' +
    //     '☼    ☼' +
    //     '☼☼☼☼☼☼';
    //   const board = new Board(boardAsString);
    //   const state: SnakeState = new SnakeState();
    //   const player: Snake = board.getPlayer();
    //   player.setState(state);
    //   const near: BoardObject[] = board.getNearMovable(player.head.coordinates, player, 1);
    //   expect(near).toEqual([
    //     { coordinates: { x: 4, y: 1 }, element: ' ' },
    //     { coordinates: { x: 3, y: 2 }, element: '●' },
    //   ]);
    // });

    // it('should not return stones for snake with size 4', () => {
    //   const boardAsString =
    //     '☼☼☼☼☼☼' +
    //     '☼╔═► ☼' +
    //     '☼╙ ● ☼' +
    //     '☼    ☼' +
    //     '☼    ☼' +
    //     '☼☼☼☼☼☼';
    //   const board = new Board(boardAsString);
    //   const state: SnakeState = new SnakeState();
    //   const player: Snake = board.getPlayer();
    //   player.setState(state);
    //   const near: BoardObject[] = board.getNearMovable(player.head.coordinates, player, 1);
    //   expect(near).toEqual([{ coordinates: { x: 4, y: 1 }, element: ' ' }]);
    // });

    // it('should return stones for snake with size 4 and fury', () => {
    //   const boardAsString =
    //     '☼☼☼☼☼☼' +
    //     '☼╔═♥ ☼' +
    //     '☼╙ ● ☼' +
    //     '☼    ☼' +
    //     '☼    ☼' +
    //     '☼☼☼☼☼☼';
    //   const board = new Board(boardAsString);
    //   const state: SnakeState = new SnakeState();
    //   state.furyDuration = 1;
    //   const player: Snake = board.getPlayer();
    //   player.setState(state);
    //   const near: BoardObject[] = board.getNearMovable(player.head.coordinates, player, 1);
    //   expect(near).toEqual([
    //     { coordinates: { x: 4, y: 1 }, element: ' ' },
    //     { coordinates: { x: 3, y: 2 }, element: '●' },
    //   ]);
    // });
  });

  describe('setElement', () => {
    it('should set element in given position', () => {
      const boardAsString =
        '☼☼☼☼☼☼' +
        '☼╔═♥ ☼' +
        '☼╙ ● ☼' +
        '☼    ☼' +
        '☼    ☼' +
        '☼☼☼☼☼☼';
      const board = new Board(boardAsString);
      const coordinates = { x: 3, y: 3 };
      board.setElement(coordinates, Element.FURY_PILL);
      expect(board.getElementByCoordinates(coordinates)).toBe(ELEMENT.FURY_PILL)
    });
  });

  describe('getEnemiesInRangeOfPlayer', () => {
    it('should return enemies', () => {
      const boardAsString =
        '☼☼☼☼☼☼' +
        '☼▲ æ ☼' +
        '☼╙ ˅ ☼' +
        '☼    ☼' +
        '☼  ×>☼' +
        '☼☼☼☼☼☼';

      const board = new Board(boardAsString);
      const enemies: Snake[] = board.getEnemiesInRangeOfPlayer(2);
      expect(enemies).toHaveLength(1);
      expect(enemies[0].head.element).toBe('˅');
    });
  });
});
