import { Board, Coordinates, MovableFilter, BoardObject } from "./board.class";
import { Snake } from "./snake.class";
import { Element, Enemy, Command } from "./constants";
import { AbstractStrategy } from './strategies/abstract-strategy.class';
import { Logger } from "./logger/logger.class";

export type MovableFilterBuilder = (distance: number) => MovableFilter;

const EnemyParts = Object.values(Enemy);
export class Pathfinder {

  private enemiesPathsCache: { [key: string]: Coordinates[] } = {};

  constructor(private readonly logger?: Logger) { }

  public findPathToCoords(board: Board, targetCoords: Coordinates, currentCoords: Coordinates, snake: Snake): Coordinates[] {
    const visited = {};
    const distances: { [key: string]: number } = {};
    distances[Board.coordinatesToString(currentCoords)] = 0;
    const path = {};
    const queue = [];
    queue.push(currentCoords);
    visited[Board.coordinatesToString(currentCoords)] = true;
    distances[Board.coordinatesToString(currentCoords)] = 0;
    let i = 0;

    while (queue.length) {
      i++;
      if (i > 1000) {
        console.log('SOMETHING WRONG, TO MANY ITERATIONS');
        console.log('trying to reach', JSON.stringify(targetCoords));
        console.log('current coord', JSON.stringify(currentCoords));
        console.log(visited);
        break;
      }
      let currentNodeCoords = queue.shift();
      const movableFilter: MovableFilter =
        this.buildMovableFilter(board, snake, distances[Board.coordinatesToString(currentNodeCoords)] + 1);
      const nearElements = board.getNearMovable(currentNodeCoords, movableFilter)
        .filter(({ coordinates }) => !visited[Board.coordinatesToString(coordinates)]);
      for (let j = 0; j < nearElements.length; j++) {
        const nearElement = nearElements[j];
        distances[Board.coordinatesToString(nearElement.coordinates)] = distances[Board.coordinatesToString(currentNodeCoords)] + 1;
        path[Board.coordinatesToString(nearElement.coordinates)] = currentNodeCoords;
        if (Board.coordinatesEqual(targetCoords, nearElement.coordinates)) {
          return this.restorePath(path, targetCoords, currentCoords);
        } else {
          visited[Board.coordinatesToString(nearElement.coordinates)] = true;
          queue.push(nearElement.coordinates);
        }
      }
    }
    return [];
  }

  public isSafePath(board: Board, path: Coordinates[], snake: Snake, enemyStrategies: AbstractStrategy[]): boolean {
    if (!this.isEscapePathAvailable(board, path, snake)) {
      return false;
    }
    if (snake.isEnemy) {
      return true;
    }

    if (this.isCollidesWithStrongerEnemy(board, path, snake, enemyStrategies)) {
      return false;
    }
    return true;
  }

  public interceptPath(board: Board, snake: Snake, currentCoords: Coordinates, path: Coordinates[]): Coordinates[] {
    for (let i = 0; i < path.length; i++) {
      const pathToEnemyPath = this.findPathToCoords(board, path[i], currentCoords, snake);
      if (pathToEnemyPath.length === i + 1 || pathToEnemyPath.length === i + 2) { // second case is for neck
        return pathToEnemyPath;
      }
    }
    return [];
  }

  private isEscapePathAvailable(board: Board, path: Coordinates[], snake: Snake): boolean {
    const newBoard: Board = board.copy();
    const newSnake: Snake = snake.copy();
    newSnake.moveToPath(newBoard, path);
    const escapePath = this.findPathToCoords(newBoard, snake.tail.coordinates, newSnake.head.coordinates, newSnake);
    return escapePath.length > 0;
  }

  private getEnemeyPath(enemy: Snake, enemyStrategies: AbstractStrategy[]): Coordinates[] {
    let enemyPath: Coordinates[] = this.enemiesPathsCache[Board.coordinatesToString(enemy.head.coordinates)];
    if (!enemyPath) {
      enemyPath = [];
      let i = 0;
      while (enemyPath.length === 0 && i < enemyStrategies.length) {
        enemyPath = enemyStrategies[i].evaluateBoard(enemy);
        i++;
      }

      this.enemiesPathsCache[Board.coordinatesToString(enemy.head.coordinates)] = enemyPath;
      if (this.logger && enemyPath.length) {
        this.logger.logEnemyPath(enemyPath);
      }
    }
    return enemyPath;
  }

  public isEnemyGetsEndOfPathFaster(board, path: Coordinates[], enemyStrategies: AbstractStrategy[]): boolean {
    const enemies: Snake[] = board.getEnemies();
    const target: Coordinates = path[path.length - 1];
    for (let i = 0; i < enemies.length; i++) {
      const enemy: Snake = enemies[i];
      const enemyPath = this.getEnemeyPath(enemy, enemyStrategies);
      let enemyTargetIdx = enemyPath.findIndex(enemyCoordinate => Board.coordinatesEqual(enemyCoordinate, target));
      if (enemyTargetIdx !== -1 && enemyTargetIdx < path.length - 1) {
        return true;
      }
    }
    return false;
  }

  private isCollidesWithStrongerEnemy(board, path: Coordinates[], snake: Snake, enemyStrategies: AbstractStrategy[]): boolean {
    const enemies: Snake[] = board.getEnemies();
    const firstMoveNear: Coordinates[] = Board.getNearCoordinatesIn(path[0], 2, true);
    for (let i = 0; i < enemies.length; i++) {
      const enemy: Snake = enemies[i];
      // CHECK NO STRONGER ENEMY HEAD IN NEXT COORDINATES NEARBY
      const enemyInRange: Coordinates = firstMoveNear.find(coordinates => Board.coordinatesEqual(coordinates, enemy.head.coordinates));
      if (enemyInRange) {
        const isStronger: boolean = snake.isStrongerThen(enemy, enemyInRange.distance);
        if (!isStronger) {
          return true;
        }
      }
      // CHECK PATH DOES NOT INTERSECTS WITH STRONGER ENEMY PATH
      let enemyPath: Coordinates[] = this.getEnemeyPath(enemy, enemyStrategies);
      for (let j = 0; j < enemyPath.length; j++) {
        // collides TODO: track neck attack
        if (
          !snake.isStrongerThen(enemy, j + 1)
          && ((path[j] && Board.coordinatesEqual(enemyPath[j], path[j]))
            || (path[j - 1] && Board.coordinatesEqual(enemyPath[j], path[j - 1]))) // neck attack
        ) {
          return true;
        }
      }
    }
    return false;
  }

  private restorePath(path, targetCoords: Coordinates, fromCoords: Coordinates): Coordinates[] {
    const restoredPath = [targetCoords];
    let currentCoords = targetCoords;
    while (true) {
      currentCoords = path[Board.coordinatesToString(currentCoords)];
      if (Board.coordinatesEqual(currentCoords, fromCoords)) {
        return restoredPath;
      }
      if (!currentCoords) {
        return null;
      }
      restoredPath.unshift(currentCoords);
    }
  }

  public buildMovableFilter(board: Board, snake: Snake, distance: number): MovableFilter {
    if (snake.isPlayer && !snake.getState()) {
      throw new Error('Player should have a state');
    }
    return function (boardObject: BoardObject, fromCoordintes: Coordinates): boolean {

      // make imposible to move in head's opposite direction (neck)
      if (
        Board.coordinatesEqual(snake.coordinates[1], boardObject.coordinates)
        && Board.coordinatesEqual(fromCoordintes, snake.head.coordinates)
      ) {
        return false;
      }

      const elements = [,
        Element.NONE,
        Element.APPLE,
        Element.FLYING_PILL,
        Element.FURY_PILL,
        Element.GOLD,
      ];

      if (snake.isPlayer && EnemyParts.includes(boardObject.element)) {
        const enemy: Snake = board.getEnemySnakeByCoordinate(boardObject.coordinates);
        return (
          enemy
          && snake.isStrongerThen(enemy, distance)
          && (snake.isFury || Board.coordinatesEqual(boardObject.coordinates, enemy.head.coordinates)) // on next tick - neck
        );
      }

      elements.push(
        Element.ENEMY_TAIL_END_DOWN,
        Element.ENEMY_TAIL_END_LEFT,
        Element.ENEMY_TAIL_END_UP,
        Element.ENEMY_TAIL_END_RIGHT,
        Element.TAIL_END_DOWN,
        Element.TAIL_END_LEFT,
        Element.TAIL_END_UP,
        Element.TAIL_END_RIGHT,
      );

      if (snake.canEatStone(distance)) {
        elements.push(Element.STONE);
      }

      return elements.includes(boardObject.element);
    }
  }
}
