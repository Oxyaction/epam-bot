/*-
 * #%L
 * Codenjoy - it's a dojo-like platform from developers to developers.
 * %%
 * Copyright (C) 2018 - 2019 Codenjoy
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
import {
    ELEMENT,
    COMMANDS
} from './constants';
import {
    Element
} from './constants';
const fetch = require('node-fetch');

// Here is utils that might help for bot development
export function getBoardAsString(board) {
    const size = getBoardSize(board);

    return getBoardAsArray(board).join("\n");
}

export function getBoardAsArray(board) {
    const size = getBoardSize(board);
    var result = [];
    for (var i = 0; i < size; i++) {
        result.push(board.substring(i * size, (i + 1) * size));
    }
    return result;
}

export function getBoardSize(board) {
    return Math.sqrt(board.length);
}

export function isGameOver(board) {
    return board.indexOf(ELEMENT.HEAD_DEAD) !== -1;
}

export function isAt(board, x, y, element) {
    if (isOutOf(board, x, y)) {
        return false;
    }
    return getAt(board, x, y) === element;
}

export function getAt(board, x, y) {
    if (isOutOf(board, x, y)) {
        return ELEMENT.WALL;
    }
    return getElementByXY(board, { x, y });
}

export function isNear(board, x, y, element) {
    if (isOutOf(board, x, y)) {
        return ELEMENT.WALL;
    }

    return isAt(board, x + 1, y, element) ||
        isAt(board, x - 1, y, element) ||
        isAt(board, x, y + 1, element) ||
        isAt(board, x, y - 1, element);
}

export function isOutOf(board, x, y) {
    const boardSize = getBoardSize(board);
    return x >= boardSize || y >= boardSize || x < 0 || y < 0;
}

export function getHeadPosition(board) {
    return getFirstPositionOf(board, [
        ELEMENT.HEAD_DOWN,
        ELEMENT.HEAD_LEFT,
        ELEMENT.HEAD_RIGHT,
        ELEMENT.HEAD_UP,
        ELEMENT.HEAD_DEAD,
        ELEMENT.HEAD_EVIL,
        ELEMENT.HEAD_FLY,
        ELEMENT.HEAD_SLEEP,
    ]);
}

export function getFirstPositionOf(board, elements) {
    for (var i = 0; i < elements.length; i++) {
        var element = elements[i];
        var position = board.indexOf(element);
        if (position !== -1) {
            return getXYByPosition(board, position);
        }
    }
    return null;
}

export function getXYByPosition(board, position) {
    if (position === -1) {
        return null;
    }

    const size = getBoardSize(board);
    return {
        x: position % size,
        y: (position - (position % size)) / size
    };
}

export function getElementByXY(board, position): Element {
    const size = getBoardSize(board);
    return board[size * position.y + position.x];
}

export function setElementByXY(board, position, element) {
    const size = getBoardSize(board);
    if (isOutOf(board, position.x, position.y)) {
        return;
    }
    const idx = size * position.y + position.x;
    return board.substr(0, idx) + element + board.substr(idx + 1);
}

export function getNear(board, { x, y }) {
    return [
        { coords: { x: x + 1, y }, element: getElementByXY(board, { x: x + 1, y }) },
        { coords: { x: x - 1, y }, element: getElementByXY(board, { x: x - 1, y }) },
        { coords: { x, y: y + 1 }, element: getElementByXY(board, { x, y: y + 1 }) },
        { coords: { x, y: y - 1 }, element: getElementByXY(board, { x, y: y - 1 }) },
    ];
}

export function isCoordNear(board, searchCoord, nearCoord) {
    const nearCoords = getNear(board, searchCoord);
    return nearCoords.some(_nearCoord => _nearCoord.coords.x === nearCoord.x && _nearCoord.coords.y === nearCoord.y);
}

export function getNearMovable(board, { x, y }, snakeLength = 2, state) {
    const nearCoords = getNear(board, { x, y });
    return nearCoords.filter(({ element }) => isMovableElement(element, snakeLength, state));
}

export function isMovableElement(element, snakeLength = 2, state) {
    const elements = [
        ELEMENT.NONE,
        ELEMENT.APPLE,
        ELEMENT.FLYING_PILL,
        ELEMENT.FURY_PILL,
        ELEMENT.GOLD
    ];

    if (snakeLength - 3 >= 2 || state === ELEMENT.FURY_PILL) {
        elements.push(ELEMENT.STONE);
    }

    return elements.includes(element);
}

export function getNextToHeadCoords(board, headPosition) {
    const head = getElementByXY(board, headPosition);
    let nextCoords;
    switch (head) {
        case ELEMENT.HEAD_DOWN:
            nextCoords = { x: headPosition.x, y: headPosition.y + 1 };
            break;
        case ELEMENT.HEAD_UP:
            nextCoords = { x: headPosition.x, y: headPosition.y - 1 };
            break;
        case ELEMENT.HEAD_LEFT:
            nextCoords = { x: headPosition.x - 1, y: headPosition.y };
            break;
        default: //right
            nextCoords = { x: headPosition.x + 1, y: headPosition.y };
            break;
    }
    return nextCoords;
}

const coordsToString = ({ x, y }) => `${x}-${y}`;

const stringToCoords = (coords) => {
    const c = coords.split('-');
    return {
        x: c[0],
        y: c[1]
    }
};

export const coordsEqual = (coords1, coords2) => coords1.x === coords2.x && coords1.y === coords2.y;

export function findPathToCoords(board, targetCoords, currentCoords, snakeLength = 2, state) {
    const visited = {};
    const distances = {};
    const path = {};
    const queue = [];
    queue.push(currentCoords);
    visited[coordsToString(currentCoords)] = true;
    distances[coordsToString(currentCoords)] = 0;
    let i = 0;
    const start = Date.now();

    while (queue.length) {
        i++;
        if (i > 1000) {
            console.log('SOMETHING WRONG, TO MANY ITERATIONS');
            console.log('board', board);
            console.log('trying to reach', JSON.stringify(targetCoords));
            console.log('current coord', JSON.stringify(currentCoords));
            console.log(visited);
            break;
        }
        let currentNodeCoords = queue.shift();
        // console.log(' ======= ');
        // console.log('iteration', i);
        // console.log('current node', JSON.stringify(currentNodeCoords));
        const nearElements = getNearMovable(board, currentNodeCoords, snakeLength, state)
            .filter(({ coords }) => !visited[coordsToString(coords)]);
        // console.log('near movable elements', JSON.stringify(nearElements));
        for (let j = 0; j < nearElements.length; j++) {
            const nearElement = nearElements[j];
            distances[coordsToString(nearElement.coords)] = distances[coordsToString(currentNodeCoords)] + 1;
            path[coordsToString(nearElement.coords)] = currentNodeCoords;
            if (coordsEqual(targetCoords, nearElement.coords)) {
                // console.log('iterations taken ', i);
                // console.log('time taken ', Date.now() - start, 'ms')
                return restorePath(path, targetCoords, currentCoords);
            } else {
                visited[coordsToString(nearElement.coords)] = true;
                queue.push(nearElement.coords);
            }
        }
    }
    return [];
}

function restorePath(path, targetCoords, fromCoords) {
    const restoredPath = [targetCoords];
    let currentCoords = targetCoords;
    const start = Date.now();
    let i = 0;
    while (true) {
        i++;
        currentCoords = path[coordsToString(currentCoords)];
        if (currentCoords.x === fromCoords.x && currentCoords.y === fromCoords.y) {
            // console.log('restore iterations taken ', i);
            // console.log('restore time taken ', Date.now() - start, 'ms');
            return restoredPath;
        }
        if (!currentCoords) {
            return null;
        }
        restoredPath.unshift(currentCoords);
    }
}

export function coordsToCommand(targetCoords, currentCoords) {
    const adjacementCoordsMapping = {
        [coordsToString({ x: currentCoords.x + 1, y: currentCoords.y })]: COMMANDS.RIGHT,
        [coordsToString({ x: currentCoords.x - 1, y: currentCoords.y })]: COMMANDS.LEFT,
        [coordsToString({ x: currentCoords.x, y: currentCoords.y + 1 })]: COMMANDS.DOWN,
        [coordsToString({ x: currentCoords.x, y: currentCoords.y - 1 })]: COMMANDS.UP,
    }
    return adjacementCoordsMapping[coordsToString(targetCoords)];
}

export function commandToCoords(currentCoords, command) {
    const commandsMapping = {
        [COMMANDS.LEFT]: { x: currentCoords.x - 1, y: currentCoords.y },
        [COMMANDS.RIGHT]: { x: currentCoords.x + 1, y: currentCoords.y },
        [COMMANDS.UP]: { x: currentCoords.x, y: currentCoords.y - 1 },
        [COMMANDS.DOWN]: { x: currentCoords.x, y: currentCoords.y + 1 },
    };
    return commandsMapping[command];
}

export function sendResetRequest() {
    const url = process.env.RESET_URL;
    fetch(url).then((res) => {
        if (!res.ok) {
            console.error(`Reset game failed, server respond with ${res.statusCode} code`);
        }
    }, (err) => {
        console.error(err);
    });
}
