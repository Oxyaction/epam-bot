/*-
 * #%L
 * Codenjoy - it's a dojo-like platform from developers to developers.
 * %%
 * Copyright (C) 2018 - 2019 Codenjoy
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
import {
    getSnakeMove2,
} from './bot';
import {
    Command,
} from './constants';
import { SnakeState } from './snake.state.class';

describe("bot", () => {
    describe('getSnakeMove2', () => {
        it("should avoid horisontal wall", () => {
            const board =
                '☼☼☼☼☼' +
                '☼   ☼' +
                '☼   ☼' +
                '☼╘═►☼' +
                '☼☼☼☼☼';
            const state: SnakeState = new SnakeState();
            const move = getSnakeMove2(board, state);
            expect(move).toEqual(Command.UP);
        });

        it("should try to catch apples", () => {
            const board =
                '☼☼☼☼☼☼' +
                '☼╘═► ☼' +
                '☼  ○ ☼' +
                '☼    ☼' +
                '☼    ☼' +
                '☼☼☼☼☼☼';
            const state: SnakeState = new SnakeState();
            const move = getSnakeMove2(board, state);
            expect(move).toEqual(Command.DOWN);
        });

        it('closest should be prioritized', () => {
            const board =
                '☼☼☼☼☼☼' +
                '☼  ○ ☼' +
                '☼ ╘► ☼' +
                '☼    ☼' +
                '☼   $☼' +
                '☼☼☼☼☼☼';
            const state: SnakeState = new SnakeState();
            const move = getSnakeMove2(board, state);
            expect(move).toEqual(Command.UP);
        });

        it('reachable should be prioritized', () => {
            const board =
                '☼☼☼☼☼☼' +
                '☼  ☼○☼' +
                '☼ ╘►☼☼' +
                '☼    ☼' +
                '☼○   ☼' +
                '☼☼☼☼☼☼';
            const state: SnakeState = new SnakeState();
            const move = getSnakeMove2(board, state);
            expect(move).toEqual(Command.DOWN);
        });

        it('should not go to dead end', () => {
            const board =
                '☼☼☼☼☼☼' +
                '☼ ╘► ☼' +
                '☼    ☼' +
                '☼☼☼○☼☼' +
                '☼ ☼ ☼☼' +
                '☼☼☼☼☼☼';

            const state: SnakeState = new SnakeState();
            state.previousCommand = Command.RIGHT;
            const move = getSnakeMove2(board, state);
            expect(move).not.toEqual(Command.DOWN);
        });

        it('should not go to dead end 2', () => {
            const board =
                '☼☼☼☼☼☼' +
                '☼    ☼' +
                '☼ ╘► ☼' +
                '☼☼☼○☼☼' +
                '☼ ☼ ☼☼' +
                '☼☼☼☼☼☼';

            const state: SnakeState = new SnakeState();
            const move = getSnakeMove2(board, state);
            expect(move).not.toEqual(Command.DOWN);
        });

        it('should escape from bigger snake', () => {
            const board =
                '☼☼☼☼☼☼☼☼' +
                '☼      ☼' +
                '☼      ☼' +
                '☼ ╘══►○☼' +
                '☼     ˄☼' +
                '☼     │☼' +
                '☼×────┘☼' +
                '☼☼☼☼☼☼☼☼';

            const state: SnakeState = new SnakeState();
            const move = getSnakeMove2(board, state);
            expect(move).not.toEqual(Command.RIGHT);
        });

        it('should attack bigger snake if fury', () => {
            const board =
                '☼☼☼☼☼☼☼☼' +
                '☼      ☼' +
                '☼      ☼' +
                '☼  ╘═♥○☼' +
                '☼     ˄☼' +
                '☼     │☼' +
                '☼×────┘☼' +
                '☼☼☼☼☼☼☼☼';

            const state: SnakeState = new SnakeState();
            state.furyDuration = 1;
            const move = getSnakeMove2(board, state);
            expect(move).toEqual(Command.RIGHT);
        });

        it('should not attack bigger snake if fury finishes fury', () => {
            const board =
                '☼☼☼☼☼☼☼☼' +
                '☼      ☼' +
                '☼      ☼' +
                '☼  ╘═♥○☼' +
                '☼     ˄☼' +
                '☼     │☼' +
                '☼×────┘☼' +
                '☼☼☼☼☼☼☼☼';

            const state: SnakeState = new SnakeState();
            state.furyDuration = 0;
            const move = getSnakeMove2(board, state);
            expect(move).not.toEqual(Command.RIGHT);
        });

        it('should not escape from smaller snake if do not die', () => {
            const board =
                '☼☼☼☼☼☼☼☼' +
                '☼      ☼' +
                '☼ ╔═╕  ☼' +
                '☼ ╚══►○☼' +
                '☼     ˄☼' +
                '☼     │☼' +
                '☼     ¤☼' +
                '☼☼☼☼☼☼☼☼';

            const state: SnakeState = new SnakeState();
            const move = getSnakeMove2(board, state);
            expect(move).toEqual(Command.RIGHT);
        });

        it('should escape from fury snake', () => {
            const board =
                '☼☼☼☼☼☼☼☼' +
                '☼      ☼' +
                '☼ ╔══╕ ☼' +
                '☼ ╚══►○☼' +
                '☼     ♣☼' +
                '☼     ¤☼' +
                '☼      ☼' +
                '☼☼☼☼☼☼☼☼';

            const state: SnakeState = new SnakeState();
            state.furyDuration = 0;
            const move = getSnakeMove2(board, state);
            expect(move).not.toEqual(Command.RIGHT);
        });

        it('should not escape from fury snake if own is fury and furyDuration enough and size is bigger', () => {
            const board =
                '☼☼☼☼☼☼☼☼' +
                '☼      ☼' +
                '☼ ╔══╕ ☼' +
                '☼ ╚══♥○☼' +
                '☼     ♣☼' +
                '☼     │☼' +
                '☼     ¤☼' +
                '☼☼☼☼☼☼☼☼';

            const state: SnakeState = new SnakeState();
            state.furyDuration = 1;
            const move = getSnakeMove2(board, state);
            expect(move).toEqual(Command.RIGHT);
        });

        it('should not escape from fury snake if own is fury and furyDuration enough and size is bigger', () => {
            const board =
                '☼☼☼☼☼☼☼☼' +
                '☼      ☼' +
                '☼ ╔══╕ ☼' +
                '☼ ╚══♥○☼' +
                '☼     ♣☼' +
                '☼     │☼' +
                '☼     ¤☼' +
                '☼☼☼☼☼☼☼☼';

            const state: SnakeState = new SnakeState();
            state.furyDuration = 1;
            const move = getSnakeMove2(board, state);
            expect(move).toEqual(Command.RIGHT);
        });

        it('should escape from fury snake if own is fury and furyDuration not enough and size is bigger', () => {
            const board =
                '☼☼☼☼☼☼☼☼' +
                '☼      ☼' +
                '☼ ╔══╕ ☼' +
                '☼ ╚══♥○☼' +
                '☼     ♣☼' +
                '☼     │☼' +
                '☼     ¤☼' +
                '☼☼☼☼☼☼☼☼';

            const state: SnakeState = new SnakeState();
            state.furyDuration = 0;
            const move = getSnakeMove2(board, state);
            expect(move).not.toEqual(Command.RIGHT);
        });

        it('should escape from fury snake if own is fury and furyDuration enough bit size is not enough', () => {
            const board =
                '☼☼☼☼☼☼☼☼' +
                '☼      ☼' +
                '☼      ☼' +
                '☼  ╘═♥○☼' +
                '☼     ♣☼' +
                '☼     ¤☼' +
                '☼      ☼' +
                '☼☼☼☼☼☼☼☼';

            const state: SnakeState = new SnakeState();
            state.furyDuration = 1;
            const move = getSnakeMove2(board, state);
            expect(move).not.toEqual(Command.RIGHT);
        });

        it('should eat first but not second stone since fury duration expires', () => {
            let board =
                '☼☼☼☼☼☼☼☼' +
                '☼      ☼' +
                '☼      ☼' +
                '☼  ╘═♥ ☼' +
                '☼    ● ☼' +
                '☼    ● ☼' +
                '☼      ☼' +
                '☼☼☼☼☼☼☼☼';

            const state: SnakeState = new SnakeState();
            state.furyDuration = 1;
            let move = getSnakeMove2(board, state);
            expect(move).toEqual(Command.DOWN);

            board =
                '☼☼☼☼☼☼☼☼' +
                '☼      ☼' +
                '☼      ☼' +
                '☼    ╓ ☼' +
                '☼    ♥ ☼' +
                '☼    ● ☼' +
                '☼      ☼' +
                '☼☼☼☼☼☼☼☼';

            move = getSnakeMove2(board, state);
            expect(move).not.toEqual(Command.DOWN);
        });

        it('should prioritize stones when in fury', () => {
            const board =
                '☼☼☼☼☼☼' +
                '☼ ╘♥●☼' +
                '☼    ☼' +
                '☼  ○ ☼' +
                '☼    ☼' +
                '☼☼☼☼☼☼';

            const state: SnakeState = new SnakeState();
            state.furyDuration = 4;
            const move = getSnakeMove2(board, state);
            expect(move).toEqual(Command.RIGHT);
        });

        it('should avoid tackles', () => {
            const board =
                '☼☼☼☼☼☼☼☼' +
                '☼   ╓  ☼' +
                '☼   ▼  ☼' +
                '☼    ˄○☼' +
                '☼   ○│ ☼' +
                '☼    │ ☼' +
                '☼×───┘ ☼' +
                '☼☼☼☼☼☼☼☼';
            const state: SnakeState = new SnakeState();
            const move = getSnakeMove2(board, state);
            expect(move).toBe(Command.LEFT);
        });
    });
});
