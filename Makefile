.PHONY: test build clean prod tag push deploy all run

TEST_PROJECT := bot-test
TEST_DIRECTORY := docker
TEST_COMPOSE_FILE := $(TEST_DIRECTORY)/docker-compose.yml

CI_USER := ec2-user
DEV_HOST := 34.201.0.89

DOCKER_REGISTRY ?= registry.gitlab.com
ORG_NAME ?= oxyaction

test:
	# Ensure building with latest base image (node) and used this cached image for consistency
	docker-compose -p $(TEST_PROJECT) --project-directory $(TEST_DIRECTORY) -f $(TEST_COMPOSE_FILE) build --pull bot-test
	docker-compose -p $(TEST_PROJECT) --project-directory $(TEST_DIRECTORY) -f $(TEST_COMPOSE_FILE) build bot-test

	docker-compose -p $(TEST_PROJECT) --project-directory $(TEST_DIRECTORY) -f $(TEST_COMPOSE_FILE) run --rm bot-test

build:
	docker-compose -p $(TEST_PROJECT) --project-directory $(TEST_DIRECTORY) -f $(TEST_COMPOSE_FILE) build bot-builder
	docker-compose -p $(TEST_PROJECT) --project-directory $(TEST_DIRECTORY) -f $(TEST_COMPOSE_FILE) up bot-builder

prod:
	docker-compose -p $(TEST_PROJECT) --project-directory $(TEST_DIRECTORY) -f $(TEST_COMPOSE_FILE) build bot
	docker-compose -p $(TEST_PROJECT) --project-directory $(TEST_DIRECTORY) -f $(TEST_COMPOSE_FILE) up --no-start bot

tag:
	docker tag $(BOT_IMAGE_ID) $(DOCKER_REGISTRY)/$(ORG_NAME)/epam-bot:latest

push:
	docker push $(DOCKER_REGISTRY)/$(ORG_NAME)/epam-bot:latest

clean:
	docker-compose -p $(TEST_PROJECT) --project-directory $(TEST_DIRECTORY) -f $(TEST_COMPOSE_FILE) kill
	docker-compose -p $(TEST_PROJECT) --project-directory $(TEST_DIRECTORY) -f $(TEST_COMPOSE_FILE) rm -f -v
	docker rmi $(docker images -q -f dangling=true) 2>/dev/null || true

deploy:
	ssh $(CI_USER)@$(DEV_HOST) < deploy.sh

all:
	make clean
	make test
	make build
	make prod
	make tag

run:
	docker-compose -p $(TEST_PROJECT) --project-directory $(TEST_DIRECTORY) -f $(TEST_COMPOSE_FILE) run --rm bot

BOT_CONTAINER_ID := $$(docker-compose -p $(TEST_PROJECT) --project-directory $(TEST_DIRECTORY) -f $(TEST_COMPOSE_FILE) ps -q bot)
BOT_IMAGE_ID := $$(docker inspect -f '{{ .Image }}' $(BOT_CONTAINER_ID))
